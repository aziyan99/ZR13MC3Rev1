//
//  UserPreferences+CoreDataProperties.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//
//

import Foundation
import CoreData


extension UserPreferences {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserPreferences> {
        return NSFetchRequest<UserPreferences>(entityName: "UserPreferences")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var isActive: Bool

}

extension UserPreferences : Identifiable {

}
