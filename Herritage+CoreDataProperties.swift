//
//  Herritage+CoreDataProperties.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//
//

import Foundation
import CoreData


extension Herritage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Herritage> {
        return NSFetchRequest<Herritage>(entityName: "Herritage")
    }

    @NSManaged public var end: Date?
    @NSManaged public var herritage_id: String?
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var plan_id: UUID?
    @NSManaged public var start: Date?
    @NSManaged public var icon: String?
    @NSManaged public var lat: Double
    @NSManaged public var long: Double

}

extension Herritage : Identifiable {

}
