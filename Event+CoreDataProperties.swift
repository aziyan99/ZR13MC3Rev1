//
//  Event+CoreDataProperties.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//
//

import Foundation
import CoreData


extension Event {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Event> {
        return NSFetchRequest<Event>(entityName: "Event")
    }

    @NSManaged public var end: Date?
    @NSManaged public var event_id: String?
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var plan_id: UUID?
    @NSManaged public var start: Date?
    @NSManaged public var icon: String?
    @NSManaged public var lat: Double
    @NSManaged public var long: Double

}

extension Event : Identifiable {

}
