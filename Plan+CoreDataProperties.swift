//
//  Plan+CoreDataProperties.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 02/08/21.
//
//

import Foundation
import CoreData


extension Plan {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Plan> {
        return NSFetchRequest<Plan>(entityName: "Plan")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var planName: String?
    @NSManaged public var planImageUrl: URL?
    @NSManaged public var startDate: Date?
    @NSManaged public var endDate: Date?
    @NSManaged public var createdAt: Date?
    @NSManaged public var duration: String?
    @NSManaged public var year: String?

}

extension Plan : Identifiable {

}
