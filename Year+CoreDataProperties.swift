//
//  Year+CoreDataProperties.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//
//

import Foundation
import CoreData


extension Year {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Year> {
        return NSFetchRequest<Year>(entityName: "Year")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var year: String?

}

extension Year : Identifiable {

}
