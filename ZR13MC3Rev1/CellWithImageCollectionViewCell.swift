//
//  CellWithImageCollectionViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithImageCollectionViewCell: UICollectionViewCell {

    static let identifier = "CellWithImageCollectionViewCell"
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    static func nib() -> UINib {
        return UINib(nibName: "CellWithImageCollectionViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        topImageView.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
    
    public func configure(imageUrl: String, title: String, subtitle: String) {
        topImageView.loadUnsplashImage(url: URL(string: imageUrl)!)
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}
