//
//  Weather+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import Foundation

struct Weather {
    var icon: String
    var date: String
    var weatherName: String

}
