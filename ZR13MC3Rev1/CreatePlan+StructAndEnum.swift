//
//  CreatePlan+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import Foundation

struct CreatePlanStruct {
    let headerTitle: String
    let footerTitle: String
    let formType: CreatePlanTypeEnum
    let form: [FormCreatePlanStruct]
}

struct FormCreatePlanStruct {
    let name: String
    let handler: (() -> Void)
}

enum CreatePlanTypeEnum {
    case datePicker
    case imagePicker
//    case lodgingPicker
}

struct CreateFormStruct {
    let headerTitle: String
    let footerTitle: String
    let formType: CreateFormTypeEnum
    let form: [FormStruct]
}

enum CreateFormTypeEnum {
    case title
    case datePicker
    case contact
}

struct FormStruct {
    let name: String
    let handler: (() -> Void)
}

