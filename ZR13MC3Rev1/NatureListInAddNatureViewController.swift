//
//  NatureListInAddNature ViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit

class NatureListInAddNatureViewController: UIViewController {
    
    var type: String = "nature"
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    var natureLists = [AddNatureStruct]()
    var eventLists = [AddEventStruct]()
    var herritageLists = [AddHerritageStruct]()
    var planId: UUID!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
        headerContainer.roundCorners([.topLeft, .topRight], radius: 10)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(LodgingListInAddLodgingTableViewCellTableViewCell.nib(), forCellReuseIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier)
        
        if type == "nature" {
            self.fetchData(url: "http://bintan.iniserver.xyz/natures") {
                self.tableView.reloadData()
            }
        }
        
        if type == "herritage" {
            self.fetchData(url: "http://bintan.iniserver.xyz/heritages") {
                self.tableView.reloadData()
            }
        }
        
        if type == "event" {
            self.fetchData(url: "http://bintan.iniserver.xyz/events") {
                self.tableView.reloadData()
            }
        }
    }
    
    private func fetchData(url: String, completionHandler: @escaping () -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print("error")
                return
            }
            
            //have data
            if self.type == "nature" {
                do {
                    self.natureLists = try JSONDecoder().decode([AddNatureStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            }
            
            if self.type == "herritage" {
                do {
                    self.herritageLists = try JSONDecoder().decode([AddHerritageStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            }
            
            if self.type == "event" {
                do {
                    self.eventLists = try JSONDecoder().decode([AddEventStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NatureListInAddNatureViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch type {
        case "nature":
            return natureLists.count
        case "event":
            return eventLists.count
        case "herritage":
            return herritageLists.count
        default:
            return natureLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier, for: indexPath) as! LodgingListInAddLodgingTableViewCellTableViewCell
        
        switch type {
        case "nature":
            cell.configure(icon: "leaf.fill", title: natureLists[indexPath.row].title!, subtitle: natureLists[indexPath.row].subTitle!)
        case "event":
            cell.configure(icon: "leaf.fill", title: eventLists[indexPath.row].title!, subtitle: eventLists[indexPath.row].subTitle!)
        case "herritage":
            cell.configure(icon: "leaf.fill", title: herritageLists[indexPath.row].title!, subtitle: herritageLists[indexPath.row].subTitle!)
        default:
            cell.configure(icon: "leaf.fill", title: natureLists[indexPath.row].title!, subtitle: natureLists[indexPath.row].subTitle!)
        }
        
        return cell
    }
}

extension NatureListInAddNatureViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch type {
        case "nature":
            let storyboard = UIStoryboard(name: "NatureDetailsInAddNature", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "NatureDetailsInAddNatureViewController") as! NatureDetailsInAddNatureViewController
            viewController.planId = planId
            viewController.type = "nature"
            viewController.natureDetail = natureLists[indexPath.row]
            self.present(viewController, animated: true, completion: nil)
        case "event":
            let storyboard = UIStoryboard(name: "NatureDetailsInAddNature", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "NatureDetailsInAddNatureViewController") as! NatureDetailsInAddNatureViewController
            viewController.planId = planId
            viewController.type = "event"
            viewController.eventDetail = eventLists[indexPath.row]
            self.present(viewController, animated: true, completion: nil)
        case "herritage":
            let storyboard = UIStoryboard(name: "NatureDetailsInAddNature", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "NatureDetailsInAddNatureViewController") as! NatureDetailsInAddNatureViewController
            viewController.planId = planId
            viewController.type = "herritage"
            viewController.herritageDetail = herritageLists[indexPath.row]
            self.present(viewController, animated: true, completion: nil)
        default:
            print("default")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch type {
        case "nature": return 120
        case "event": return 120
        case "herritage": return 180
        default: return 120
        }
    }
}
