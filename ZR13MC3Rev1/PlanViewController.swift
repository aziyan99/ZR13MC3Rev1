//
//  PlanViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 03/08/21.
//

import UIKit

class PlanViewController: UIViewController {
    
    var plan: Plan!
    var planId: UUID!
    var planItems: [PlanItemsStruct] = [PlanItemsStruct]()
    @IBOutlet weak var topBackgroundImageView: UIImageView!
    @IBOutlet weak var planWeatherButton: UIButton!
    @IBOutlet weak var itineraryDetailButton: UIButton!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var startEndDateContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var planDurationLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var itineraryLabel: UILabel!
    
   
    
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.height
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        prepareData()
        // ga tau kenapa kalau largetitle nya true gambarnya jadi keatas
//        navigationController?.navigationBar.prefersLargeTitles = false
        //navigationController?.navigationBar.isTranslucent = true
        //navigationController?.navigationBar.setBackgroundTransparent()
        
        //background color
//        view.backgroundColor = .systemGray6
        itineraryLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        startDateLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        endDateLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        let roundedFont: UIFont
           if let descriptor = UIFont.systemFont(ofSize: 15, weight: .semibold).fontDescriptor.withDesign(.rounded) {
               roundedFont = UIFont(descriptor: descriptor, size: 15)
           } else {
               roundedFont = UIFont.systemFont(ofSize: 15, weight: .semibold)
           }

        itineraryLabel.font = roundedFont
        DispatchQueue.main.async {
            self.topBackgroundImageView.loadUnsplashImage(url: self.plan.planImageUrl!)
            self.topBackgroundImageView.addoverlay()
            //            self.topBackgroundImageView.image = UIImage(named: "imgtes1")
        }
        
        topBackgroundImageView.contentMode = UIView.ContentMode.scaleAspectFill
        view.addSubview(topBackgroundImageView)
        view.sendSubviewToBack(topBackgroundImageView)
        
        //setup weather button
        planWeatherButton.layer.cornerRadius = 5
        
        //start and end date container view
        startEndDateContainerView.layer.cornerRadius = 5
        startEndDateContainerView.dropShadow()
//        startEndDateContainerView.backgroundColor = .white
        
        //substitution the plan data
        planNameLabel.text = plan.planName
        planDurationLabel.text = "\(formingPlanDuration(duration: plan.duration!)) to"
        startDateLabel.text = "\(formingDateToString(date: plan.startDate!))"
        endDateLabel.text = "\(formingDateToString(date: plan.endDate!))"
        
        //table view
        tableView.register(PlanTableViewCell.nib(), forCellReuseIdentifier: PlanTableViewCell.identifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func prepareData() {
        let lodgingData = LodgingCoreDataHelper.filterByPlanId(id: planId)
        let natureData = NatureCoreDataHelper.filterByPlanId(id: planId)
        let herritageData = HerritageCoreDataHelper.filterByPlanId(id: planId)
        let eventData = EventCoreDataHelper.filterByPlanId(id: planId)
        
        var lodgingItems = [PlanComponentsStruct]()
        var activityItems = [PlanComponentsStruct]()
        
        planItems = [PlanItemsStruct]()
        
        for item in lodgingData {
            lodgingItems.append(PlanComponentsStruct(id: item.id!, icon: "bed.double.fill", title: item.name!, subtitle: "2 Night", type: .data, kindOf: "lodging", lat: item.lat, long: item.long))
        }
        
        for item in natureData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "nature", lat: item.lat, long: item.long))
        }
        
        for item in herritageData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "herritage", lat: item.lat, long: item.long))
        }
        
        for item in eventData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "event", lat: item.lat, long: item.long))
        }
        
        lodgingItems.append(PlanComponentsStruct(id: UUID(), icon: "", title: "Add Lodging...", subtitle: "", type: .action, kindOf: "action", lat: 0.918550, long: 104.466507))
        activityItems.append(PlanComponentsStruct(id: UUID(), icon: "", title: "Add Activity...", subtitle: "", type: .action, kindOf: "action", lat: 0.918550, long: 104.466507))
        
        if lodgingItems.count < 2 {
            planItems.append(
                PlanItemsStruct(header: "Place to Stay", footer: "Telling us where you'd like to stay will help us optimize your trip plans when you use our Autoplanner", items: lodgingItems)
            )
        } else {
            planItems.append(
                PlanItemsStruct(header: "Lodgings", footer: "", items: lodgingItems)
            )
        }
        planItems.append(
            PlanItemsStruct(header: "Activities", footer: "", items: activityItems)
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.sizeToFit()
        navigationController?.navigationBar.tintColor = .white
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)
    }
    
    
    @IBAction func didTapItineraryDetailButton(_ sender: Any) {
        print("detail")
        let storyboard = UIStoryboard(name: "ItineraryDetail", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "ItineraryDetailViewController") as! ItineraryDetailViewController
        viewController.planId = planId
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func didTapWeatherButton(_ sender: Any) {
        let weather: [Weather] = [
            Weather(icon: "cloud.bolt.rain.fill", date: "27 July 2021", weatherName: "AM Thunderstrom"),
            Weather(icon: "cloud.fill", date: "28 July 2021", weatherName: "Mostly Cloudy"),
        ]
        
        let weatherViewController = WeatherViewController()
        weatherViewController.weatherConditions = weather
        weatherViewController.modalPresentationStyle = .custom
        weatherViewController.transitioningDelegate = self
        present(weatherViewController, animated: true, completion: nil)
    }
    
    @objc func reloadTableView() {
        prepareData()
        tableView.reloadData()
    }
    
    private func handleDeleteLodging(index: IndexPath, lodgingId: UUID) {
        let optionMenu = UIAlertController(title: nil, message: "Delete this lodging?", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
            let lodgingData = LodgingCoreDataHelper.getById(id: lodgingId)
            if lodgingData.count > 0 {
                LodgingCoreDataHelper.deleteLodging(lodging: lodgingData[0])
            }
            self.reloadTableView()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func handleDeleteActivity(index: IndexPath, kindOf: String, activityId: UUID) {
        let optionMenu = UIAlertController(title: nil, message: "Delete this activity?", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
            if kindOf == "nature" {
                let natureData = NatureCoreDataHelper.getById(id: activityId)
                if natureData.count > 0 {
                    NatureCoreDataHelper.deleteNature(nature: natureData[0])
                }
            }
            if kindOf == "herritage" {
                let herritageData = HerritageCoreDataHelper.getById(id: activityId)
                if herritageData.count > 0 {
                    HerritageCoreDataHelper.deleteHerritage(herritage: herritageData[0])
                }
            }
            if kindOf == "event" {
                let eventData = EventCoreDataHelper.getById(id: activityId)
                if eventData.count > 0 {
                    EventCoreDataHelper.deleteEvent(event: eventData[0])
                }
            }
            self.reloadTableView()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
}


extension PlanViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension PlanViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return planItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch planItems[indexPath.section].items[indexPath.row].type {
        case .action:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = planItems[indexPath.section].items[indexPath.row].title
            return cell
        case .data:
            let cell = tableView.dequeueReusableCell(withIdentifier: PlanTableViewCell.identifier, for: indexPath) as! PlanTableViewCell
            cell.configure(with: planItems[indexPath.section].items[indexPath.row], type: planItems[indexPath.section].header)
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
}

extension PlanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // 0 = lodging, 1 = activity
        switch planItems[indexPath.section].items[indexPath.row].type {
        case .data:
            print("data")
        case.action:
            if indexPath.section == 0 {
                //add lodging
                let addLodgingStoryboard = UIStoryboard(name: "AddLodgingToPlan", bundle: nil)
                let addLodgingViewController = addLodgingStoryboard.instantiateViewController(identifier: "AddLodgingToPlanViewController") as! AddLodgingToPlanViewController
                addLodgingViewController.planId = planId
                present(addLodgingViewController, animated: true, completion: nil)
            } else {
                let actionSheet = UIAlertController(title: "", message: "Add Activity", preferredStyle: .actionSheet)
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
               
                 
                //if choose auto planner
                let autoPlanner = UIAlertAction(title: "Autoplanner", style: .default) {_ in
                    // check if he user was have lodging
                    let alert = UIAlertController(title: "Itinerary Not Found", message: "You must specify at least one place to stay or one activity to do", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }
                
                //if choose add manually
                let addManually = UIAlertAction(title: "Add Manually", style: .default) {_ in
                    let addActivityOptionViewController = AddActivityOptionsViewController()
                    addActivityOptionViewController.modalPresentationStyle = .custom
                    addActivityOptionViewController.transitioningDelegate = self
                    addActivityOptionViewController.planId = self.planId
                    self.present(addActivityOptionViewController, animated: true, completion: nil)
                }
                let image = UIImage(systemName: "bolt.badge.a.fill")
                
                
                autoPlanner.setValue(image, forKey: "image")
                autoPlanner.setValue(1, forKey: "titleTextAlignment")
                
                autoPlanner.setValue(UIColor(named: "ColorRed"), forKey: "titleTextColor")
                actionSheet.addAction(autoPlanner)
                actionSheet.addAction(addManually)
                actionSheet.addAction(cancelAction)
              
                   
                self.present(actionSheet, animated: true, completion: nil)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return planItems[section].header
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return planItems[section].footer
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch planItems[indexPath.section].items[indexPath.row].type {
        case .data: return 62
        case .action: return 42
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if planItems[indexPath.section].header == "Lodgings" {
            let deleteLodging = UIContextualAction(style: .destructive, title: "") { [weak self] (action, view, completionHandler) in
                self?.handleDeleteLodging(index: indexPath, lodgingId: self!.planItems[indexPath.section].items[indexPath.row].id)
                completionHandler(true)
            }
            deleteLodging.image = UIImage(systemName: "trash.fill")
            deleteLodging.backgroundColor = .systemRed
            let configuration = UISwipeActionsConfiguration(actions: [deleteLodging])
            return configuration
        } else {
            let deleteActivity = UIContextualAction(style: .destructive, title: "") { [weak self] (action, view, completionHandler) in
                self?.handleDeleteActivity(index: indexPath, kindOf: self!.planItems[indexPath.section].items[indexPath.row].kindOf, activityId: self!.planItems[indexPath.section].items[indexPath.row].id)
                completionHandler(true)
            }
            deleteActivity.image = UIImage(systemName: "trash.fill")
            deleteActivity.backgroundColor = .systemRed
            let configuration = UISwipeActionsConfiguration(actions: [deleteActivity])
            return configuration
        }
    }
}

//extension PlanViewController: UIScrollViewDelegate {
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        print("scrolllll")
//
//
//    }
    


//}
