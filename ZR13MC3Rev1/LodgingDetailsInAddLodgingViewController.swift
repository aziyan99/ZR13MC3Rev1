//
//  LodgingDetailsInAddLodgingViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 08/08/21.
//

import UIKit

struct AddLodgingStructHelpers {
    var header: String
    var type: AddLodgingEnumHelpers
}

enum AddLodgingEnumHelpers {
    case image
    case desc
    case recommendation
    case activities
    case contact
    case map
    case accomodation
    case attraction
}

class LodgingDetailsInAddLodgingViewController: UIViewController {
    
    var items: [AddLodgingStructHelpers] = [
        AddLodgingStructHelpers(header: "", type: .image),
        AddLodgingStructHelpers(header: "", type: .desc),
        AddLodgingStructHelpers(header: "", type: .recommendation),
        AddLodgingStructHelpers(header: "", type: .activities),
        AddLodgingStructHelpers(header: "", type: .contact),
        AddLodgingStructHelpers(header: "", type: .map),
        AddLodgingStructHelpers(header: "Nearby Accomodations", type: .accomodation),
        AddLodgingStructHelpers(header: "Nearby Attractions", type: .attraction)
    ]
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    var lodgingDetail: AddLodgingStruct!
    var planId: UUID!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)        
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(CellWithImageTableViewCell.self, forCellReuseIdentifier: CellWithImageTableViewCell.identifier)
        tableView.register(CellWithDescTableViewCell.self, forCellReuseIdentifier: CellWithDescTableViewCell.identifier)
        tableView.register(CellWithTwoContainerViewTableViewCell.nib(), forCellReuseIdentifier: CellWithTwoContainerViewTableViewCell.identifier)
        tableView.register(CellWithBestActivitiesTodoTableViewCell.nib(), forCellReuseIdentifier: CellWithBestActivitiesTodoTableViewCell.identifier)
        tableView.register(CellWithContactTableViewCell.nib(), forCellReuseIdentifier: CellWithContactTableViewCell.identifier)
        tableView.register(CellWithMapTableViewCell.nib(), forCellReuseIdentifier: CellWithMapTableViewCell.identifier)
        tableView.register(CellWithCellTableViewCell.nib(), forCellReuseIdentifier: CellWithCellTableViewCell.identifier)
        tableView.register(CellWithCollectionViewTableViewCell.nib(), forCellReuseIdentifier: CellWithCollectionViewTableViewCell.identifier)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapAddToLodgingButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddLodgingForm", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "AddLodgingFormViewController") as! AddLodgingFormViewController
        viewController.lodgingDetails = lodgingDetail
        viewController.planId = planId
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension LodgingDetailsInAddLodgingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch items[section].type {
        case .image:
            return 1
        case .desc:
            return 1
        case .recommendation:
            return 1
        case .activities:
            return 1
        case .contact:
            return 1
        case .map:
            return 1
        case .accomodation:
            return lodgingDetail.nearbyAccomodation.count
        case .attraction:
            return lodgingDetail.nearbyAttraction.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch items[indexPath.section].type {
        case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithImageTableViewCell.identifier, for: indexPath) as! CellWithImageTableViewCell
            cell.configure(image: lodgingDetail.image!, name: lodgingDetail.title!, star: 5)
            return cell
        case .desc:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithDescTableViewCell.identifier, for: indexPath) as! CellWithDescTableViewCell
            cell.configure(title: lodgingDetail.title!, subtitle: lodgingDetail.description!)
            return cell
        case .recommendation:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithTwoContainerViewTableViewCell.identifier, for: indexPath) as! CellWithTwoContainerViewTableViewCell
            cell.configure(recommendedActivity: lodgingDetail.recommendActivities!, bestView: lodgingDetail.bestView!)
            return cell
        case .activities:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithBestActivitiesTodoTableViewCell.identifier, for: indexPath) as! CellWithBestActivitiesTodoTableViewCell
            
            return cell
        case .contact:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
            cell.configure(phoneNumber: lodgingDetail.contact!, website: lodgingDetail.website!)
            return cell
        case .map:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithMapTableViewCell.identifier, for: indexPath) as! CellWithMapTableViewCell
            cell.configure(name: lodgingDetail.title!, lat: lodgingDetail.latLong[0].lat!, long: lodgingDetail.latLong[0].long!)
            return cell
        case .accomodation:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCellTableViewCell.identifier, for: indexPath) as! CellWithCellTableViewCell
            cell.configure(icon: lodgingDetail.nearbyAccomodation[indexPath.row].image!, title: lodgingDetail.nearbyAccomodation[indexPath.row].label!, subtitle: lodgingDetail.nearbyAccomodation[indexPath.row].place!)
            return cell
        case .attraction:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCollectionViewTableViewCell.identifier, for: indexPath) as! CellWithCollectionViewTableViewCell
            cell.configure(model: lodgingDetail.nearbyAttraction)
            return cell
        }
    }
}

extension LodgingDetailsInAddLodgingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch items[indexPath.section].type {
        case .image:
            return 613
        case .desc:
            return 284
        case .recommendation:
            return 120
        case .activities:
            return 280
        case .contact:
            return 200
        case .map:
            return 200
        case .accomodation:
            return 98
        case .attraction:
            return 226
        }
    }
}
