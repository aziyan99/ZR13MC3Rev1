//
//  CellWithWeatherButtonTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithWeatherButtonTableViewCell: UITableViewCell {
    static let identifier = "CellWithWeatherButtonTableViewCell"
    @IBOutlet weak var iconContainerView: UIView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
//    @IBOutlet weak var weatherContainerView: UIView!
//    @IBOutlet weak var weatherButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleContainerView.layer.cornerRadius = 5
        iconContainerView.layer.cornerRadius = 15
        
        titleContainerView.backgroundColor = .systemBackground
        iconContainerView.backgroundColor = .systemRed
        iconImageView.tintColor = .systemBackground
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithWeatherButtonTableViewCell", bundle: nil)
    }
    
    public func configure(title: String, subtitle: String, icon: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        iconImageView.image = UIImage(systemName: icon)
    }
}
