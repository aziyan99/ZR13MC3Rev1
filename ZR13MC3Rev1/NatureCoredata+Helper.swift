//
//  Nature.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import Foundation
import UIKit
import CoreData

struct NatureCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newNature() -> Nature {
            let nature = NSEntityDescription.insertNewObject(forEntityName: "Nature", into: context) as! Nature
            return nature
    }
    
    static func saveNature() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deleteNature(nature: Nature) {
        context.delete(nature)
        saveNature()
    }
    
    static func retrieveNatures() -> [Nature] {
        do {
            let fetchRequest = NSFetchRequest<Nature>(entityName: "Nature")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func filterByPlanId(id: UUID) -> [Nature] {
        do {
            let predicate = NSPredicate(format: "plan_id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Nature>(entityName: "Nature")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func getById(id: UUID) -> [Nature] {
        do {
            let predicate = NSPredicate(format: "id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Nature>(entityName: "Nature")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
