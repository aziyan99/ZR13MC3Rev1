//
//  CellWithImageAndSubtitleTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import UIKit

class CellWithImageAndSubtitleTableViewCell: UITableViewCell {
    static let identifier = "CellWithImageAndSubtitleTableViewCell"
    
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        topImageView.addoverlay()
        titleLabel.textColor = .white
        subtitleLabel.textColor = .white
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithImageAndSubtitleTableViewCell", bundle: nil)
    }
    
    public func configure(imageUrl: String, title: String, subtitle: String) {
        DispatchQueue.main.async {
            self.topImageView.loadUnsplashImage(url: URL(string: imageUrl)!)
        }
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
}
