//
//  CreatePlanViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import UIKit
import UnsplashPhotoPicker

class CreatePlanViewController: UIViewController {
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var planImageView: UIImageView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var createButton: UIBarButtonItem!
    @IBOutlet weak var planNameTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
  
    var planName: String!
    var planImageUrl :  URL! = URL(string: "https://images.unsplash.com/photo-1573880302212-983d60276465?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80")!
    var startDate: Date!
    var endDate: Date!
    var createdAt: Date!
    var uuid: UUID!
    
    var planDate: String!
    
    var createPlanForm: [CreatePlanStruct] = [
        CreatePlanStruct(headerTitle: "Time you have", footerTitle: "", formType: .datePicker, form: [
            FormCreatePlanStruct(name: "Starts", handler: {
                print("Start date")
            }),
            FormCreatePlanStruct(name: "Ends", handler: {
                print("End date")
            })
        ]),
//        CreatePlanStruct(headerTitle: "Place to stay", footerTitle: "Telling us where you'd like to stay will help us optimize your trip plans when you use our Autoplanner", formType: .lodgingPicker, form: [
//            FormCreatePlanStruct(name: "Add lodging...", handler: {
//                print("lodging picker")
//            })
//        ]),
        CreatePlanStruct(headerTitle: "Picture to remember", footerTitle: "", formType: .imagePicker, form: [
            FormCreatePlanStruct(name: "Cover image", handler: {
                print("image picker")
            })
        ])
        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDidLayoutSubviews()
        //disable constraint warning
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        
        navigationBar.setBackgroundTransparent()//make navigationbar transparent
//        planImageView.image = UIImage(named: "imgtes1")
        planImageView.loadUnsplashImage(url: URL(string: "https://images.unsplash.com/photo-1573880302212-983d60276465?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80")!)
        planImageView.addoverlay()// darken the image
    
        
        //change bar button color
        cancelButton.tintColor = .white
        createButton.tintColor = .white
        
        //table view
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CreatePlanWithDatePickerTableViewCell.nib(), forCellReuseIdentifier: CreatePlanWithDatePickerTableViewCell.identifier)
        tableView.register(CreatePlanWithImagePickerTableViewCell.nib(), forCellReuseIdentifier: CreatePlanWithImagePickerTableViewCell.identifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidLayoutSubviews() {
        
        //isScrollEnabled of table view  should be dissable because our table is inside scrollview.
        tableView.isScrollEnabled = false
        
        
        //if above tableView.contentSize.height   not zero and giving acurate value then proceed further and update our parent scroll view contentsize height for height.
//        print(tableView.contentSize.height)
        
        
        
        //place some bottom peeding as you want
        let bottomPedding:CGFloat = 30
        let imgaeHeigh:CGFloat = 455
        
        //Finally update your scrollview content size with newly created table height + bottom pedding.
        scrollview.contentSize = CGSize.init(width: scrollview.contentSize.width, height:tableView.contentSize.height + bottomPedding + imgaeHeigh)
       
//        print(scrollview.contentSize.height)
        
    }
    
    @IBAction func didTapCancelbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapCreateButton(_ sender: Any) {
        // set planName to planNameTextField value
        planName = planNameTextField.text
        let startAndEndDateDiff = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        let duration = startAndEndDateDiff.day
        let year = getPlanYear(start: startDate, end: endDate)
        
        // saving to coredata
        let plan = PlanCoreDataHelper.newPlan()
        plan.id = UUID()
        plan.planName = planName
        plan.planImageUrl = planImageUrl
        plan.startDate = startDate
        plan.endDate = endDate
        plan.createdAt = Date()
        plan.duration = "\(duration ?? 0)"
        plan.year = year
        PlanCoreDataHelper.savePlan()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newPlanAdded"), object: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapStartsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        startDate = senderDate!.date
    }
    
    @objc func didTapEndsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        endDate = senderDate!.date
    }
    
    
}

extension CreatePlanViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return createPlanForm.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return createPlanForm[section].form.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let models = createPlanForm[indexPath.section]
        switch models.formType {
        case .datePicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithDatePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithDatePickerTableViewCell
//            cell.configure(model: models.form[indexPath.row])
            cell.configure(title: models.form[indexPath.row].name)
            //check which date picker clicked
            if models.form[indexPath.row].name == "Starts" {
                cell.datePickerItem.addTarget(self, action: #selector(didTapStartsDatePicker(_ :)), for: .valueChanged)
            } else {
                cell.datePickerItem.addTarget(self, action: #selector(didTapEndsDatePicker(_ :)), for: .valueChanged)
            }
            return cell
        case .imagePicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithImagePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithImagePickerTableViewCell
            cell.configure(model: models.form[indexPath.row])
            cell.accessoryType = .disclosureIndicator
            return cell
//        case .lodgingPicker:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//            cell.textLabel?.text = models.form[indexPath.row].name
//            cell.accessoryType = .disclosureIndicator
//            return cell
        }
    }
}

extension CreatePlanViewController: UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        planNameTextField.resignFirstResponder()
        
        tableView.deselectRow(at: indexPath, animated: true)
        switch createPlanForm[indexPath.section].formType {
        case .datePicker:
            print("date picker")
        case .imagePicker:
            let configuration = UnsplashPhotoPickerConfiguration(
                accessKey: "dDlP-tg5Ds3EC-aAnMuKIJQeR-_rzE6EPO6C5Xmw90Y",
                secretKey: "ESov0foJ_Ftfh-oF6XT-1rzXmSH3TFx2Z2xCkJ8AX30",
                allowsMultipleSelection: false
            )
            let photoPicker = UnsplashPhotoPicker(configuration: configuration)
            photoPicker.photoPickerDelegate = self
            
            present(photoPicker, animated: true, completion: nil)
//        case .lodgingPicker:
//            print("lodging picker")
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return createPlanForm[section].headerTitle
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return createPlanForm[section].footerTitle
    }
   
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        let systemFont = UIFont.systemFont(ofSize: 15, weight: .semibold)
        
        header.textLabel?.font = systemFont
        let roundedFont: UIFont
            if let descriptor = systemFont.fontDescriptor.withDesign(.rounded) {
                roundedFont = UIFont(descriptor: descriptor, size: 15)
            } else {
                roundedFont = systemFont
            }
        header.textLabel?.font = roundedFont
        header.textLabel?.textColor = UIColor.label
        
    }
    
   
}

extension CreatePlanViewController: UnsplashPhotoPickerDelegate {
    func unsplashPhotoPicker(_ photoPicker: UnsplashPhotoPicker, didSelectPhotos photos: [UnsplashPhoto]) {
        planImageUrl = photos[0].urls[.regular]!
        planImageView.loadUnsplashImage(url: planImageUrl)
    }
    
    func unsplashPhotoPickerDidCancel(_ photoPicker: UnsplashPhotoPicker) {
        print("Cancel select photo")
    }
}
