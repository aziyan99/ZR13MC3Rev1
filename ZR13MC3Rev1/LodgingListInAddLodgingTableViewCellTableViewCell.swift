//
//  LodgingListInAddLodgingTableViewCellTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 06/08/21.
//

import UIKit

class LodgingListInAddLodgingTableViewCellTableViewCell: UITableViewCell {

    static let identifier = "LodgingListInAddLodgingTableViewCellTableViewCell"
    
    @IBOutlet weak var iconContainerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconContainerView.layer.cornerRadius = 15
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "LodgingListInAddLodgingTableViewCellTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
    
    public func configure(icon: String = "bed.double.fill", title: String, subtitle: String) {
        iconImageView.image = UIImage(systemName: icon)
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
}
