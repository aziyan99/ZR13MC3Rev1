//
//  AddActivityOptionCollectionViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 06/08/21.
//

import UIKit

class AddActivityOptionCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "AddActivityOptionCollectionViewCell"
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.cornerRadius = 5
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "AddActivityOptionCollectionViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
        textLabel.text = nil
    }
    
    public func configure(with model: AddActivityOptionStruct) {
        iconImageView.image = UIImage(systemName: model.icon)
        textLabel.text = model.title
    }
}
