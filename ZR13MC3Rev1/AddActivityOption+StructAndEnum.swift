//
//  AddActivityOption+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 06/08/21.
//

struct AddActivityOptionStruct {
    var icon: String
    var title: String
    var action: AddActivityOptionEnum
}

enum AddActivityOptionEnum {
    case nature
    case event
    case herritage
}
