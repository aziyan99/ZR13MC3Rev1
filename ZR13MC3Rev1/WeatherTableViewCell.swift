//
//  WeatherTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    static let identifier = "WeatherTableViewCell"

    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "WeatherTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        weatherIconImageView.image = nil
        dateLabel.text = nil
        weatherNameLabel.text = nil
    }
    
    public func configure(with model: Weather) {
        weatherIconImageView.image = UIImage(systemName: model.icon)
        dateLabel.text = model.date
        weatherNameLabel.text = model.weatherName
    }
    
}
