//
//  CellWithImageTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit

class CellWithImageTableViewCell: UITableViewCell {

    static let identifier = "CellWithImageTableViewCell"
    
    let topImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.addoverlay()
        return imageView
    }()
    
    let titleLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        label.numberOfLines = 0
        return label
    }()
    
    let star1: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star")
        imageView.tintColor = .white
        
        return imageView
    }()
    
    let star2: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star")
        imageView.tintColor = .white
        
        return imageView
    }()
    
    let star3: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star")
        imageView.tintColor = .white
        
        return imageView
    }()
    
    let star4: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star")
        imageView.tintColor = .white
        
        return imageView
    }()
    
    let star5: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star")
        imageView.tintColor = .white
        
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(topImageView)
        topImageView.addSubview(titleLabel)
        topImageView.addSubview(star1)
        topImageView.addSubview(star2)
        topImageView.addSubview(star3)
        topImageView.addSubview(star4)
        topImageView.addSubview(star5)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        topImageView.frame = contentView.bounds
        titleLabel.frame = CGRect(x: 16, y: 19, width: topImageView.frame.size.width, height: topImageView.frame.size.height / 4)
        
        star1.frame = CGRect(x: 8,
                             y: titleLabel.frame.size.height + 8,
                             width: 16,
                             height: 16)
        star2.frame = CGRect(x: star1.frame.size.width + 8,
                             y: titleLabel.frame.size.height + 8,
                             width: 16,
                             height: 16)
        star3.frame = CGRect(x: star1.frame.size.width + star2.frame.size.width + 8,
                             y: titleLabel.frame.size.height + 8,
                             width: 16,
                             height: 16)
        star4.frame = CGRect(x: star1.frame.size.width + star2.frame.size.width + star3.frame.size.width + 8,
                             y: titleLabel.frame.size.height + 8,
                             width: 16,
                             height: 16)
        star5.frame = CGRect(x: star1.frame.size.width + star2.frame.size.width + star3.frame.size.width + star4.frame.size.width + 8,
                             y: titleLabel.frame.size.height + 8,
                             width: 16,
                             height: 16)
    }
    
    public func configure (image: String, name: String, star: Int) {
        topImageView.loadUnsplashImage(url: URL(string: image)!)
        titleLabel.text = name
        switch star {
        case 1:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star")
            star3.image = UIImage(systemName: "star")
            star4.image = UIImage(systemName: "star")
            star5.image = UIImage(systemName: "star")
        case 2:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star.fill")
            star3.image = UIImage(systemName: "star")
            star4.image = UIImage(systemName: "star")
            star5.image = UIImage(systemName: "star")
        case 3:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star.fill")
            star3.image = UIImage(systemName: "star.fill")
            star4.image = UIImage(systemName: "star")
            star5.image = UIImage(systemName: "star")
        case 4:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star.fill")
            star3.image = UIImage(systemName: "star.fill")
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star")
        case 5:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star.fill")
            star3.image = UIImage(systemName: "star.fill")
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star.fill")
        default:
            star1.image = UIImage(systemName: "star.fill")
            star2.image = UIImage(systemName: "star.fill")
            star3.image = UIImage(systemName: "star.fill")
            star4.image = UIImage(systemName: "star.fill")
            star5.image = UIImage(systemName: "star.fill")
        }
    }
    
    
}
