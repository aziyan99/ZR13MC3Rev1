//
//  AddLodgingFormViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit

class AddLodgingFormViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var type: String!
    var lodgingDetails: AddLodgingStruct!
    
    var planId: UUID!
    var lodgingId: String!
    var startDate: Date!
    var endDate: Date!
    var uuid: UUID!
    
    var form: [CreateFormStruct] = [
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .title, form: [
            FormStruct(name: "", handler: {
                print("title")
            })
        ]),
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .datePicker, form: [
            FormStruct(name: "Check-in", handler: {
                print("check in")
            }),
            FormStruct(name: "Check-out", handler: {
                print("check out")
            })
        ]),
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .contact, form: [
            FormStruct(name: "", handler: {
                print("contact")
            })
        ])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CreatePlanWithDatePickerTableViewCell.nib(), forCellReuseIdentifier: CreatePlanWithDatePickerTableViewCell.identifier)
        tableView.register(CellWithWeatherButtonTableViewCell.nib(), forCellReuseIdentifier: CellWithWeatherButtonTableViewCell.identifier)
        tableView.register(CellWithContactTableViewCell.nib(), forCellReuseIdentifier: CellWithContactTableViewCell.identifier)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        let save = LodgingCoreDataHelper.newLodging()
        save.name = lodgingDetails.title
        save.id = UUID()
        save.plan_id = planId
        save.lodging_id = lodgingDetails._id
        save.start = startDate
        save.end = endDate
        save.lat = lodgingDetails.latLong[0].lat!
        save.long = lodgingDetails.latLong[0].long!
        LodgingCoreDataHelper.saveLodging()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapStartsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        startDate = senderDate!.date
    }
    
    @objc func didTapEndsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        endDate = senderDate!.date
    }
}

extension AddLodgingFormViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return form.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return form[section].form.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch form[indexPath.section].formType {
        case .title:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithWeatherButtonTableViewCell.identifier, for: indexPath) as! CellWithWeatherButtonTableViewCell
            cell.configure(title: lodgingDetails.title!, subtitle: lodgingDetails.subTitle!, icon: "bed.double.fill")
            return cell
        case .datePicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithDatePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithDatePickerTableViewCell
            cell.configure(title: form[indexPath.section].form[indexPath.row].name)
            
            if form[indexPath.section].form[indexPath.row].name == "Check-in" {
                cell.datePickerItem.addTarget(self, action: #selector(didTapStartsDatePicker(_ :)), for: .valueChanged)
            } else {
                cell.datePickerItem.addTarget(self, action: #selector(didTapEndsDatePicker(_ :)), for: .valueChanged)
            }
            
            return cell
        case .contact:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
            cell.configure(phoneNumber: lodgingDetails.contact!, website: lodgingDetails.website!)
            return cell
        }
    }
}

extension AddLodgingFormViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch form[indexPath.section].formType {
        case .title: return 108
        case .datePicker: return 90
        case .contact: return 200
        }
    }
}
