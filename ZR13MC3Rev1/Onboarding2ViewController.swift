//
//  OnboardingViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import UIKit

class Onboarding2ViewController: UIViewController {

    @IBOutlet weak var startBurron: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        startBurron.layer.cornerRadius = 5
    }
    
    @IBAction func didTapStartButton(_ sender: Any) {
        prepareData()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTabBarController = mainStoryboard.instantiateViewController(identifier: "MainTabBarController")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
        isFirstTime.set(false, forKey: keyFirstTime)
    }
    
    
    func prepareData() {
        let save1 = UserPreferencesCoreDataHelper.newUserPreferences()
        save1.id = UUID()
        save1.name = "Slow and Easy"
        save1.type = "activity"
        save1.isActive = true
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //2
        let save2 = UserPreferencesCoreDataHelper.newUserPreferences()
        save2.id = UUID()
        save2.name = "Medium"
        save2.type = "activity"
        save2.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //3
        let save3 = UserPreferencesCoreDataHelper.newUserPreferences()
        save3.id = UUID()
        save3.name = "Fast-paced"
        save3.type = "activity"
        save3.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //4
        let save4 = UserPreferencesCoreDataHelper.newUserPreferences()
        save4.id = UUID()
        save4.name = "Culture"
        save4.type = "attraction"
        save4.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //5
        let save5 = UserPreferencesCoreDataHelper.newUserPreferences()
        save5.id = UUID()
        save5.name = "Romantic"
        save5.type = "attraction"
        save5.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //6
        let save6 = UserPreferencesCoreDataHelper.newUserPreferences()
        save6.id = UUID()
        save6.name = "Outdoors"
        save6.type = "attraction"
        save6.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //7
        let save7 = UserPreferencesCoreDataHelper.newUserPreferences()
        save7.id = UUID()
        save7.name = "Natures"
        save7.type = "attraction"
        save7.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //8
        let save8 = UserPreferencesCoreDataHelper.newUserPreferences()
        save8.id = UUID()
        save8.name = "Beaches"
        save8.type = "attraction"
        save8.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        //9
        let save9 = UserPreferencesCoreDataHelper.newUserPreferences()
        save9.id = UUID()
        save9.name = "Heritages"
        save9.type = "attraction"
        save9.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
        
        
        //10
        let save10 = UserPreferencesCoreDataHelper.newUserPreferences()
        save10.id = UUID()
        save10.name = "Relaxing"
        save10.type = "attraction"
        save10.isActive = false
        UserPreferencesCoreDataHelper.saveUserPreferences()
    }
}
