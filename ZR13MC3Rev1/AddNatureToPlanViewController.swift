//
//  AddNatureToPlanViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit
import MapKit

class AddNatureToPlanViewController: UIViewController {
    
    @IBOutlet weak var filterContainerView: UIView!
    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var type: String = "nature"
    var coordinate = [MapViewStruct]()
    var planId: UUID!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)
        
        filterContainerView.dropShadow()
        filterContainerView.layer.cornerRadius = 5
        
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 60
        let mapWidth:CGFloat = view.frame.size.width-20
        let mapHeight:CGFloat = 300
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        let initialLocation = CLLocation(latitude: 0.918550, longitude: 104.466507)
        let region = MKCoordinateRegion(center: initialLocation.coordinate, latitudinalMeters: 700000, longitudinalMeters: 700000)
        mapView.setRegion(region, animated: true)
        
        mapView.delegate = self
        
        
        DispatchQueue.main.async { [self] in
            let viewController = NatureListInAddNatureViewController()
            viewController.modalPresentationStyle = .custom
            viewController.transitioningDelegate = self
            viewController.type = type
            viewController.planId = planId
            self.present(viewController, animated: true, completion: nil)
            
            if type == "nature" {
                self.fetchData(url: "http://bintan.iniserver.xyz/natures") {
                    // mapping pin to the map
                    var annotation = [MKAnnotation]()
                    for item in coordinate {
                        let artwork = Artwork(
                            title: item.title,
                            locationName: item.locationName,
                            discipline: "Sculpture",
                            coordinate: CLLocationCoordinate2D(latitude: item.lat!, longitude: item.long!))
                        annotation.append(artwork)
                    }
                    mapView.showAnnotations(annotation, animated: true)
                    
                }
            }
            
            if type == "herritage" {
                self.fetchData(url: "http://bintan.iniserver.xyz/heritages") {
                    // mapping pin to the map
                    var annotation = [MKAnnotation]()
                    for item in coordinate {
                        let artwork = Artwork(
                            title: item.title,
                            locationName: item.locationName,
                            discipline: "Sculpture",
                            coordinate: CLLocationCoordinate2D(latitude: item.lat!, longitude: item.long!))
                        annotation.append(artwork)
                    }
                    mapView.showAnnotations(annotation, animated: true)
                }
            }
        }
    }
    
    private func fetchData(url: String, completionHandler: @escaping () -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print("error")
                return
            }
            
            //have data
            if self.type == "nature" {
                var response: [AddNatureStruct]!
                do {
                    response = try JSONDecoder().decode([AddNatureStruct].self, from: data)
                    for nature in response {
                        self.coordinate.append(MapViewStruct(lat: nature.latLong[0].lat, long: nature.latLong[0].long, title: nature.title, locationName: nature.subTitle))
                    }
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            }
            
            if self.type == "herritage" {
                var response: [AddHerritageStruct]!
                do {
                    response = try JSONDecoder().decode([AddHerritageStruct].self, from: data)
                    for nature in response {
                        self.coordinate.append(MapViewStruct(lat: nature.latLong[0].lat, long: nature.latLong[0].long, title: nature.title, locationName: nature.subTitle))
                    }
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapFilterButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LodgingFilter", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "LodgingFilterViewController") as! LodgingFilterViewController
        viewController.filterDelegate = self
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddNatureToPlanViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension AddNatureToPlanViewController: MKMapViewDelegate {
    // 1
    func mapView(
        _ mapView: MKMapView,
        viewFor annotation: MKAnnotation
    ) -> MKAnnotationView? {
        // 2
        guard let annotation = annotation as? Artwork else {
            return nil
        }
        // 3
        let identifier = "artwork"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView(
            withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(
                annotation: annotation,
                reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}

extension AddNatureToPlanViewController: FilterChooseDelegate {
    func didTapChoice(itemName: String, itemId: String, itemType: String) {
        filterNameLabel.text = itemName
        viewDidLoad()
    }
}
