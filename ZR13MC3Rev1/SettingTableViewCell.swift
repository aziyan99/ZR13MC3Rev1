//
//  SettingTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    static let identifier = "SettingTableViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageContainerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "SettingTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        iconImageView.image = nil
        iconImageContainerView.backgroundColor = nil
    }
    
    public func configure (title: String, isActive: Bool, type: String) {
        titleLabel.text = title
        if isActive {
            iconImageView.image = UIImage(systemName: "checkmark")
            iconImageContainerView.backgroundColor = .systemRed
            iconImageView.tintColor = .systemBackground
        } else {
            if type == "activity" {
                iconImageView.image = UIImage(systemName: "plus")
                iconImageContainerView.backgroundColor = .systemBackground
                iconImageView.tintColor = .systemBackground
            } else {
                iconImageView.image = UIImage(systemName: "plus")
                iconImageContainerView.backgroundColor = .systemBackground
                iconImageContainerView.layer.borderWidth = 1
                iconImageContainerView.layer.borderColor = UIColor.systemGray4.cgColor
                iconImageView.tintColor = .black
            }
        }
    }
    
}
