//
//  PlanActivities+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import Foundation

struct PlanItemsStruct {
    var header: String
    var footer: String
    var items: [PlanComponentsStruct]
}

struct PlanComponentsStruct {
    var id: UUID
    var icon: String
    var title: String
    var subtitle: String
    var type: PlanItemCellTypeOptions
    var kindOf: String
    var lat: Double
    var long: Double
}

enum PlanItemCellTypeOptions {
    case data
    case action
}
