//
//  CellWithContactTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit

class CellWithContactTableViewCell: UITableViewCell {
    
    static let identifier = "CellWithContactTableViewCell"

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 5
        containerView.dropShadow()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithContactTableViewCell", bundle: nil)
    }
    
    public func configure (phoneNumber: String, website: String) {
        phoneNumberLabel.text = phoneNumber
        websiteLabel.text = website
    }
}
