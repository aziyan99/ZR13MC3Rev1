//
//  FirstTimeCheck.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import Foundation

let isFirstTime = UserDefaults.standard
let keyFirstTime = "First time huh"

func isFirstTimeHuh() -> Bool {
    return !isFirstTime.bool(forKey: keyFirstTime)
}
