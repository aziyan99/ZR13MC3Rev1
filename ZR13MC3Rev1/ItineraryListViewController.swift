//
//  ItineraryListViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import UIKit

class ItineraryListViewController: UIViewController {

    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    var planId: UUID!
    
    var planItems: [PlanItemsStruct] = [PlanItemsStruct]()
    
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
        headerContainer.layer.cornerRadius = 25
        
        prepareData()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PlanTableViewCell.nib(), forCellReuseIdentifier: PlanTableViewCell.identifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)

        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }

        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)

        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
    
    func prepareData() {
        let lodgingData = LodgingCoreDataHelper.filterByPlanId(id: planId)
        let natureData = NatureCoreDataHelper.filterByPlanId(id: planId)
        let herritageData = HerritageCoreDataHelper.filterByPlanId(id: planId)
        let eventData = EventCoreDataHelper.filterByPlanId(id: planId)
        
        var activityItems = [PlanComponentsStruct]()
        
        planItems = [PlanItemsStruct]()
        
        for item in lodgingData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: "bed.double.fill", title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "lodging", lat: item.lat, long: item.long))
        }
        
        for item in natureData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "nature", lat: item.lat, long: item.long))
        }
        
        for item in herritageData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "herritage", lat: item.lat, long: item.long))
        }
        
        for item in eventData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "event", lat: item.lat, long: item.long))
        }
        
        planItems.append(
            PlanItemsStruct(header: "Activities", footer: "", items: activityItems)
        )
    }
}

extension ItineraryListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PlanTableViewCell.identifier, for: indexPath) as! PlanTableViewCell
        cell.configure(with: planItems[indexPath.section].items[indexPath.row], type: planItems[indexPath.section].header)
        return cell
    }
}

extension ItineraryListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
}

