//
//  NavigationBar+Extension.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import Foundation
import UIKit

extension UINavigationBar {
    public func setBackgroundTransparent() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.backgroundColor = .clear
        if #available(iOS 13.0, *) {
            self.standardAppearance.backgroundColor = .clear
            self.standardAppearance.backgroundEffect = .none
            self.standardAppearance.shadowColor = .clear
        }
    }
}
