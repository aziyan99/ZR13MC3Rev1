//
//  UIView+Extension.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import Foundation
import UIKit

extension UIView {
    
    // instantiate the parent navigation controller
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    //This function will add a layer on any `UIView` to make that `UIView` look darkened
    func addoverlay(color: UIColor = .black, alpha: CGFloat = 0.6, cornerRadius: CGFloat = 0) {
        let overlay = UIView()
        overlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlay.layer.cornerRadius = cornerRadius
        overlay.frame = bounds
        overlay.backgroundColor = UIColor(named: "ColorDark") 
        overlay.alpha = alpha
        addSubview(overlay)
    }
    
    //make top background image to top edge of screen
    func addBackground(with url: URL) {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let _ = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: 455))
        imageViewBackground.loadUnsplashImage(url: url)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }
    
    //shadow
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor(named: "ColorDark")?.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
   
}
