//
//  Date+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 02/08/21.
//

import Foundation

func formingPlanDuration(duration: String) -> String {
    return "\(duration) days"
}

func formingStartToEndDate(start: Date, end: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMM"
    let startDate = formatter.string(from: start)
    let endDate = formatter.string(from: end)
    return "\(startDate) - \(endDate)"
}

func formingDateToString(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMM YYYY"
    let stringDate = formatter.string(from: date)
    return "\(stringDate)"
}

func getPlanYear(start: Date, end: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "YYYY"
    let startYear = formatter.string(from: start)
    let endYear = formatter.string(from: end)
    if startYear == endYear {
        return startYear
    } else {
        return "0000"
    }
}

func formingDateToStringWithTwoSection(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd MMM"
    let stringDate = formatter.string(from: date)
    return "\(stringDate)"
}
