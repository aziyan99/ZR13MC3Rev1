//
//  LodgingFilterViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit
import MapKit
import CoreLocation

protocol FilterChooseDelegate {
    func didTapChoice(itemName: String, itemId: String, itemType: String)
}

class LodgingFilterViewController: UIViewController {
    
    @IBOutlet weak var myLocationContainerView: UIView!
    @IBOutlet weak var myLocationIconContainer: UIView!
    @IBOutlet weak var myLocationLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var filterDelegate: FilterChooseDelegate!
    
    var selectedId: String!
    var selectedName: String!
    var selectedIndexPath: IndexPath!
    var lodgingLists = [AddLodgingStruct]()
    var eventLists = [AddEventStruct]()
    var natureLists = [AddNatureStruct]()
    var herritageLists = [AddHerritageStruct]()
    var type: String = "event"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myLocationContainerView.layer.cornerRadius = 5
        myLocationContainerView.dropShadow()
        myLocationContainerView.backgroundColor = .systemBackground
        myLocationIconContainer.layer.cornerRadius = 15
        
        fetchData {
            self.tableView.reloadData()
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(LodgingListInAddLodgingTableViewCellTableViewCell.nib(), forCellReuseIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier)
    }
    
    private func fetchData(completionHandler: @escaping () -> ()) {
        if type == "event" {
            let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/events")!, completionHandler: { (data, response, error) in
                guard let data = data, error == nil else {
                    print("error")
                    return
                }
                
                //have data
                do {
                    self.eventLists = try JSONDecoder().decode([AddEventStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            })
            task.resume()
        }
        
        if type == "lodging" {
            let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/lodgings")!, completionHandler: { (data, response, error) in
                guard let data = data, error == nil else {
                    print("error")
                    return
                }
                
                //have data
                do {
                    self.lodgingLists = try JSONDecoder().decode([AddLodgingStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            })
            task.resume()
        }
        
        if type == "nature" {
            let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/natures")!, completionHandler: { (data, response, error) in
                guard let data = data, error == nil else {
                    print("error")
                    return
                }
                
                //have data
                do {
                    self.natureLists = try JSONDecoder().decode([AddNatureStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            })
            task.resume()
        }
        
        if type == "herritage" {
            let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/heritages")!, completionHandler: { (data, response, error) in
                guard let data = data, error == nil else {
                    print("error")
                    return
                }
                
                //have data
                do {
                    self.herritageLists = try JSONDecoder().decode([AddHerritageStruct].self, from: data)
                    DispatchQueue.main.async {
                        completionHandler()
                    }
                } catch {
                    print("Failed to convert \(error.localizedDescription)")
                }
            })
            task.resume()
        }
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        filterDelegate.didTapChoice(itemName: selectedName, itemId: selectedId, itemType: type)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSegmentedChange(_ sender: Any) {
        
        if selectedIndexPath != nil {
            tableView.cellForRow(at: selectedIndexPath)?.accessoryType = .none
        }
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            type = "event"
        case 1:
            type = "lodging"
        case 2:
            type = "nature"
        case 3:
            type = "herritage"
        default:
            type = "event"
        }
        
        fetchData {
            self.tableView.reloadData()
        }
    }
}

extension LodgingFilterViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return eventLists.count
        case 1:
            return lodgingLists.count
        case 2:
            return natureLists.count
        case 3:
            return herritageLists.count
        default:
            return eventLists.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier, for: indexPath) as! LodgingListInAddLodgingTableViewCellTableViewCell
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            //event
            cell.configure(icon: "ticket.fill", title: eventLists[indexPath.row].title!, subtitle: eventLists[indexPath.row].subTitle!)
        case 1:
            //lodging
            cell.configure(icon: "bed.double.fill", title: lodgingLists[indexPath.row].title!, subtitle: lodgingLists[indexPath.row].subTitle!)
        case 2:
            //nature
            cell.configure(icon: "leaf.fill", title: natureLists[indexPath.row].title!, subtitle: natureLists[indexPath.row].subTitle!)
        case 3:
            //herritage
            cell.configure(icon: "pyramid.fill", title: herritageLists[indexPath.row].title!, subtitle: herritageLists[indexPath.row].subTitle!)
        default:
            //event
            cell.configure(icon: "ticket.fill", title: eventLists[indexPath.row].title!, subtitle: eventLists[indexPath.row].subTitle!)
        }
        
        return cell
    }
}

extension LodgingFilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedId != nil || selectedName != nil || selectedIndexPath != nil {
            selectedId = nil
            selectedName = nil
            tableView.cellForRow(at: selectedIndexPath)?.accessoryType = .none
        }
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedIndexPath = indexPath
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            selectedId = eventLists[indexPath.row]._id
            selectedName = eventLists[indexPath.row].title
        case 1:
            selectedId = lodgingLists[indexPath.row]._id
            selectedName = lodgingLists[indexPath.row].title
        case 2:
            selectedId = natureLists[indexPath.row]._id
            selectedName = natureLists[indexPath.row].title
        case 3:
            selectedId = herritageLists[indexPath.row]._id
            selectedName = herritageLists[indexPath.row].title
        default:
            selectedId = eventLists[indexPath.row]._id
            selectedName = eventLists[indexPath.row].title
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            return 120
        case 1:
            return 98
        case 2:
            return 98
        case 3:
            return 160
        default:
            return 100
        }
    }
}
