//
//  CellWithTwoContainerViewTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithTwoContainerViewTableViewCell: UITableViewCell {
    
    static let identifier = "CellWithTwoContainerViewTableViewCell"

    @IBOutlet weak var leftContainerView: UIView!
    @IBOutlet weak var rightContainerView: UIView!
    @IBOutlet weak var recommendedActivityLabel: UILabel!
    @IBOutlet weak var bestViewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rightContainerView.dropShadow()
        leftContainerView.dropShadow()
        rightContainerView.layer.cornerRadius = 5
        leftContainerView.layer.cornerRadius = 5
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithTwoContainerViewTableViewCell", bundle: nil)
    }
    
    public func configure(recommendedActivity: String, bestView: String) {
        recommendedActivityLabel.text = recommendedActivity
        bestViewLabel.text = bestView
    }
}
