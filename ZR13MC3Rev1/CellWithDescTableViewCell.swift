//
//  CellWithDescTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit

class CellWithDescTableViewCell: UITableViewCell {
    
    static let identifier = "CellWithDescTableViewCell"
    
    let containerView: UIView = {
        let uiView = UIView()
        uiView.dropShadow()
        uiView.backgroundColor = .systemBackground
        uiView.layer.cornerRadius = 5
        return uiView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let divider: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = .systemGray5
        return uiView
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .systemGray6
        contentView.addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(divider)
        containerView.addSubview(subtitleLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.frame = CGRect(x: 20, y: 8, width: contentView.frame.size.width - 40, height: contentView.frame.size.height - 16)
        titleLabel.frame = CGRect(x: 16, y: 8, width: containerView.frame.size.width - 32, height: containerView.frame.size.height / 4)
        divider.frame = CGRect(x: 16, y: titleLabel.frame.size.height - 8, width: titleLabel.frame.size.width, height: 1)
        subtitleLabel.frame = CGRect(x: 16, y: titleLabel.frame.size.height + 8, width: divider.frame.size.width, height: containerView.frame.size.height - (titleLabel.frame.size.height + 32))
    }
    
    public func configure(title: String, subtitle: String) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
}
