//
//  ItineraryDetailViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit
import MapKit

class ItineraryDetailViewController: UIViewController {

    @IBOutlet weak var filterContainerView: UIView!
    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var planId: UUID!
    var planItems: [PlanItemsStruct] = [PlanItemsStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterContainerView.dropShadow()
        filterContainerView.layer.cornerRadius = 5
        
        prepareData()
        
        DispatchQueue.main.async { [self] in
            let viewController = ItineraryListViewController()
            viewController.modalPresentationStyle = .custom
            viewController.transitioningDelegate = self
            viewController.planId = self.planId
            self.present(viewController, animated: true, completion: nil)
            
            var annotation = [MKAnnotation]()
            for item in planItems[0].items {
                let artwork = Artwork(
                    title: item.title,
                    locationName: item.subtitle,
                  discipline: "Sculpture",
                    coordinate: CLLocationCoordinate2D(latitude: item.lat, longitude: item.long))
                annotation.append(artwork)
            }
            mapView.showAnnotations(annotation, animated: true)
            mapView.mapType = MKMapType.standard
            mapView.isZoomEnabled = true
            mapView.isScrollEnabled = true
            mapView.delegate = self
        }
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func prepareData() {
        let lodgingData = LodgingCoreDataHelper.filterByPlanId(id: planId)
        let natureData = NatureCoreDataHelper.filterByPlanId(id: planId)
        let herritageData = HerritageCoreDataHelper.filterByPlanId(id: planId)
        let eventData = EventCoreDataHelper.filterByPlanId(id: planId)
        
        var activityItems = [PlanComponentsStruct]()
        
        planItems = [PlanItemsStruct]()
        
        for item in lodgingData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: "bed.double.fill", title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "lodging", lat: item.lat, long: item.long))
        }
        
        for item in natureData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "nature", lat: item.lat, long: item.long))
        }
        
        for item in herritageData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "herritage", lat: item.lat, long: item.long))
        }
        
        for item in eventData {
            activityItems.append(PlanComponentsStruct(id: item.id!, icon: item.icon!, title: item.name!, subtitle: formingDateToStringWithTwoSection(date: item.start!), type: .data, kindOf: "event", lat: item.lat, long: item.long))
        }
        
        planItems.append(
            PlanItemsStruct(header: "Activities", footer: "", items: activityItems)
        )
    }
}

extension ItineraryDetailViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension ItineraryDetailViewController: MKMapViewDelegate {
    // 1
    func mapView(
        _ mapView: MKMapView,
        viewFor annotation: MKAnnotation
    ) -> MKAnnotationView? {
        // 2
        guard let annotation = annotation as? Artwork else {
            return nil
        }
        // 3
        let identifier = "artwork"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView (
            withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(
                annotation: annotation,
                reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}
