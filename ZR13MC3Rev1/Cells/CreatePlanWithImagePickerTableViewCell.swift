//
//  CreatePlanWithImagePickerTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import UIKit

class CreatePlanWithImagePickerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoIconImageView: UIImageView!
    @IBOutlet weak var formNameLabel: UILabel!
    
    var handler: (() -> Void)!
    
    static let identifier = "CreatePlanWithImagePickerTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
//        photoIconImageView.tintColor = .black
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CreatePlanWithImagePickerTableViewCell", bundle: nil)
    }
    
    public func configure(model: FormCreatePlanStruct) {
        formNameLabel.text = model.name
        handler = model.handler
    }
}
