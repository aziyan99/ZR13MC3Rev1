//
//  CreatePlanWithDatePickerTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 01/08/21.
//

import UIKit

class CreatePlanWithDatePickerTableViewCell: UITableViewCell {

    @IBOutlet weak var dateIconImageView: UIImageView!
    @IBOutlet weak var formNameLabel: UILabel!
    @IBOutlet weak var datePickerItem: UIDatePicker!
    
    var date: String!
    
    static let identifier = "CreatePlanWithDatePickerTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        dateIconImageView.tintColor = .black
        
        datePickerItem.translatesAutoresizingMaskIntoConstraints = false
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CreatePlanWithDatePickerTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        datePickerItem.date = Date()
        formNameLabel.text = nil
        
    }
    
    public func configure(title: String) {
        formNameLabel.text = title
    }
}
