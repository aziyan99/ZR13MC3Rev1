//
//  SendDataBack+Protocol.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import Foundation

protocol SendDataBackInFilterDelegate {
    func didSendData(itemName: String, itemId: String, itemType: String)
}
