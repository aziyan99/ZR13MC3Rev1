//
//  EventCoredata+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import Foundation
import UIKit
import CoreData

struct EventCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newEvent() -> Event {
            let event = NSEntityDescription.insertNewObject(forEntityName: "Event", into: context) as! Event
            return event
    }
    
    static func saveEvent() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deleteEvent(event: Event) {
        context.delete(event)
        saveEvent()
    }
    
    static func retrieveEvents() -> [Event] {
        do {
            let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func filterByPlanId(id: UUID) -> [Event] {
        do {
            let predicate = NSPredicate(format: "plan_id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func getById(id: UUID) -> [Event] {
        do {
            let predicate = NSPredicate(format: "id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
