//
//  PlanTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import UIKit

class PlanTableViewCell: UITableViewCell {
    
    static let identifier = "PlanTableViewCell"
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var iconImageContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        iconImageView.tintColor = .white
//        iconImageContainerView.layer.cornerRadius = 5
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "PlanTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
    
    public func configure(with model: PlanComponentsStruct, type: String) {
        iconImageView.image = UIImage(systemName: model.icon)
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        if type == "Lodgings" {
//            iconImageContainerView.backgroundColor = .systemRed
        } else {
//            iconImageContainerView.backgroundColor = .systemIndigo
        }
    }
}
