//
//  AddActivityOptionsViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 05/08/21.
//

import UIKit

class AddActivityOptionsViewController: UIViewController {
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    var options: [AddActivityOptionStruct] = [
        AddActivityOptionStruct(icon: "leaf.fill", title: "Natures", action: .nature),
        AddActivityOptionStruct(icon: "ticket.fill", title: "Events", action: .event),
        AddActivityOptionStruct(icon: "pyramid.fill", title: "Herritage", action: .herritage)
    ]
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    var planId: UUID!
    
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        layout.sectionInset = UIEdgeInsets(top: 16, left: 10, bottom: 0, right: 10)
        layout.itemSize = CGSize(width: view.frame.size.width/3, height: view.frame.size.width/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        super.viewDidLoad()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
        headerContainer.roundCorners([.topLeft, .topRight], radius: 10)
        
        collectionView!.collectionViewLayout = layout
        collectionView.register(AddActivityOptionCollectionViewCell.nib(), forCellWithReuseIdentifier: AddActivityOptionCollectionViewCell.identifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddActivityOptionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AddActivityOptionCollectionViewCell.identifier, for: indexPath) as! AddActivityOptionCollectionViewCell
        cell.configure(with: options[indexPath.row])
        cell.layer.cornerRadius = 5
        cell.dropShadow()
//        cell.backgroundColor = UIColor(named: "ColorLight")
        return cell
    }
}

extension AddActivityOptionsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 116, height: 88)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch options[indexPath.row].action {
        case .nature:
            let storyboard = UIStoryboard(name: "AddNatureToPlan", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "AddNatureToPlanViewController") as! AddNatureToPlanViewController
            viewController.planId = planId
            viewController.type = "nature"
            present(viewController, animated: true, completion: nil)
        case .event:
            let storyboard = UIStoryboard(name: "AddNatureToPlan", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "AddNatureToPlanViewController") as! AddNatureToPlanViewController
            viewController.planId = planId
            viewController.type = "event"
            present(viewController, animated: true, completion: nil)
        case .herritage:
            let storyboard = UIStoryboard(name: "AddNatureToPlan", bundle: nil)
            let viewController = storyboard.instantiateViewController(identifier: "AddNatureToPlanViewController") as! AddNatureToPlanViewController
            viewController.planId = planId
            viewController.type = "herritage"
            present(viewController, animated: true, completion: nil)
        }
    }
}
