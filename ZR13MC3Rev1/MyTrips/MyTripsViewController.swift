//
//  MyTripsViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 30/07/21.
//

import UIKit

class MyTripsViewController: UIViewController {
    
    let KEPRI_COVID19_WEBSITE: String = "https://corona.kepriprov.go.id/"
    var plans: [Plan]!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "My Trips"

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddButton))
        navigationController?.navigationBar.isTranslucent = true
        
        //load data from coredata
        plans = PlanCoreDataHelper.retrievePlans()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(MyTripsTableViewCell.nib(), forCellReuseIdentifier: MyTripsTableViewCell.identifier)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: NSNotification.Name(rawValue: "newPlanAdded"), object: nil)
        
        restructurePlansData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.sizeToFit()
        navigationController?.navigationBar.tintColor = UIColor(named: "ColorRed") 
    }
    
    @objc func didTapAddButton() {
        // covid-19 action sheet guidance
        let optionMenu = UIAlertController(title: nil, message: "Covid-19 Guidance", preferredStyle: .actionSheet)
        let readAction = UIAlertAction(title: "Read it!", style: .default, handler: {_ in
            guard let url = URL(string: self.KEPRI_COVID19_WEBSITE) else { return }
            UIApplication.shared.open(url) //open kepri covid19 guidance
        })
        // navigate to create plan view controller
        let createPlanAction = UIAlertAction(title: "Create plan", style: .default, handler: {_ in
            let createPlanStoryboard = UIStoryboard(name: "CreatePlan", bundle: nil)
            let createPlanViewController = createPlanStoryboard.instantiateViewController(identifier: "CreatePlanViewController") as! CreatePlanViewController
            self.present(createPlanViewController, animated: true, completion: nil)
//            navigationController?.pushViewController(createPlanViewController, animated: true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(readAction)
        optionMenu.addAction(createPlanAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func reloadTableView() {
        viewDidLoad()
        tableView.reloadData()
    }
    
    private func handleDeletePlan(index: Int) {
        let optionMenu = UIAlertController(title: nil, message: "Delete this plan?", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
            PlanCoreDataHelper.deletePlan(plan: self.plans[index])
            self.reloadTableView()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    private func restructurePlansData() {
        var rawPlans: [Plan]
        var availableYears = [String]()
        var preparePlans = [[Plan]]()
        
        rawPlans = PlanCoreDataHelper.retrievePlans()
        
        for plan in rawPlans {
            if !availableYears.contains(plan.year!) {
                availableYears.append(plan.year!)
            }
        }
        
//        let grouped = Dictionary(grouping: rawPlans) { (plan) in
//            return plan.year
//        }

//        print(grouped)
        
        var initCount = 0
        for _ in availableYears {
            for plan in rawPlans{
                if preparePlans.count == 0 {
                    preparePlans.append([plan])
                } else {
                    if preparePlans.contains([plan]) {
                        preparePlans[initCount].append(plan)
                    } else {
                        preparePlans.append([plan])
                    }
                }
            }
            initCount += 1
        }
        
//        print(preparePlans)
    }
}

extension MyTripsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1//plans.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  plans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyTripsTableViewCell.identifier, for: indexPath) as! MyTripsTableViewCell
        cell.configure(model: plans[indexPath.row])
        return cell
    }
}

extension MyTripsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let planSortyboard = UIStoryboard(name: "Plan", bundle: nil)
        let planViewController = planSortyboard.instantiateViewController(identifier: "PlanViewController") as! PlanViewController
        planViewController.plan = plans[indexPath.row]
        planViewController.planId = plans[indexPath.row].id
        navigationController?.pushViewController(planViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deletePlan = UIContextualAction(style: .destructive,
                                            title: "Delete") { [weak self] (action, view, completionHandler) in
            self?.handleDeletePlan(index: indexPath.row)
            completionHandler(true)
        }
        deletePlan.backgroundColor = .systemRed
        let configuration = UISwipeActionsConfiguration(actions: [deletePlan])
        return configuration
    }
}
