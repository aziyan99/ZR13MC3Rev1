//
//  MyTripsTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 02/08/21.
//

import UIKit

class MyTripsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    
    static let identifier = "MyTripsTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundImageView.addoverlay()
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "MyTripsTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundImageView.image = nil
        dateLabel.text = nil
        planNameLabel.text = nil
        durationLabel.text = nil
    }
    
    public func configure(model: Plan) {
        DispatchQueue.global(qos: .userInteractive).async {
            self.backgroundImageView.loadUnsplashImage(url: model.planImageUrl!)
        }
        dateLabel.text = "In \(formingStartToEndDate(start: model.startDate!, end: model.endDate!))"
        planNameLabel.text = model.planName
        durationLabel.text = formingPlanDuration(duration: model.duration!)
    }
    
}
