//
//  SettingViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 06/08/21.
//

import UIKit

struct UserPreferencesStruct {
    var header: String
    var type: String
    var items: [UserPreferencesItemsStruct]
}

struct UserPreferencesItemsStruct {
    var name: String
    var isActive: Bool
}

class SettingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var userPreferencesData = [UserPreferences]()
    
    var userPreferences = [UserPreferencesStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Setting"
        
        var activityPreferences = [UserPreferencesItemsStruct]()
        var attractionPreferences = [UserPreferencesItemsStruct]()
        userPreferencesData = UserPreferencesCoreDataHelper.retrieveUserPreferences()
        for preferences in userPreferencesData {
            if preferences.type == "activity" {
                activityPreferences.append(UserPreferencesItemsStruct(name: preferences.name!, isActive: preferences.isActive))
            } else {
                attractionPreferences.append(UserPreferencesItemsStruct(name: preferences.name!, isActive: preferences.isActive))
            }
        }
        
        userPreferences = [
            UserPreferencesStruct(header: "Activities Preferences ", type: "activity", items: activityPreferences),
            UserPreferencesStruct(header: "Attractions Preferences ", type: "attraction", items: attractionPreferences)
        ]
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(SettingTableViewCell.nib(), forCellReuseIdentifier: SettingTableViewCell.identifier)
    }
    
    func updateUserPreferences(name: String, isActive: Bool) {
        let result = UserPreferencesCoreDataHelper.filterByName(name: name)
        if result.count != 0 {
            result[0].setValue(isActive, forKey: "isActive")
        }
        UserPreferencesCoreDataHelper.saveUserPreferences()
    }
}

extension SettingViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return userPreferences.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userPreferences[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingTableViewCell.identifier, for: indexPath) as! SettingTableViewCell
        cell.configure(title: userPreferences[indexPath.section].items[indexPath.row].name, isActive: userPreferences[indexPath.section].items[indexPath.row].isActive, type: userPreferences[indexPath.section].type)
        return cell
    }
}

extension SettingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return userPreferences[section].header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if userPreferences[indexPath.section].items[indexPath.row].isActive {
            if userPreferences[indexPath.section].type != "activity" {
                updateUserPreferences(name: userPreferences[indexPath.section].items[indexPath.row].name, isActive: false)
                userPreferences[indexPath.section].items[indexPath.row].isActive = false
            }
        } else {
            if userPreferences[indexPath.section].type == "activity" {
                for (index, _) in userPreferences[indexPath.section].items.enumerated() {
                    userPreferences[indexPath.section].items[index].isActive = false
                }
                updateUserPreferences(name: userPreferences[indexPath.section].items[indexPath.row].name, isActive: true)
                userPreferences[indexPath.section].items[indexPath.row].isActive = true
            } else {
                updateUserPreferences(name: userPreferences[indexPath.section].items[indexPath.row].name, isActive: true)
                userPreferences[indexPath.section].items[indexPath.row].isActive = true
            }
        }
        tableView.reloadData()
    }
}
