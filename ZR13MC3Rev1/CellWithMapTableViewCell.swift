//
//  CellWithMapTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import UIKit
import MapKit

class CellWithMapTableViewCell: UITableViewCell {
    
    static let identifier = "CellWithMapTableViewCell"
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.dropShadow()
        containerView.layer.cornerRadius = 5
        
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        mapView.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleContainerView.roundCorners([.topLeft, .topRight], radius: 5)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithMapTableViewCell", bundle: nil)
    }
    
    public func configure(name: String, lat: Double, long: Double) {
        let initialLocation = CLLocation(latitude: 0.918550, longitude: 104.466507)
        let region = MKCoordinateRegion(center: initialLocation.coordinate, latitudinalMeters: 50000, longitudinalMeters: 50000)
        mapView.setRegion(region, animated: false)
        
        let place = MKPointAnnotation()
        place.title = name
        place.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        mapView.showAnnotations([place], animated: false)
    }
}

extension CellWithMapTableViewCell: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}
