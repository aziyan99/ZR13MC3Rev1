//
//  CellWithBestActivitiesTodoTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithBestActivitiesTodoTableViewCell: UITableViewCell {

    static let identifier = "CellWithBestActivitiesTodoTableViewCell"
    
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.dropShadow()
        containerView.layer.cornerRadius = 5
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithBestActivitiesTodoTableViewCell", bundle: nil)
    }
    
    public func configure() {
        //
    }
    
}
