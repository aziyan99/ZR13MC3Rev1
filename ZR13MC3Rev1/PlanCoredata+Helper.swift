//
//  PlanCoredata+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 02/08/21.
//

import Foundation
import UIKit
import CoreData

struct PlanCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newPlan() -> Plan {
            let plan = NSEntityDescription.insertNewObject(forEntityName: "Plan", into: context) as! Plan
            return plan
    }
    
    static func savePlan() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deletePlan(plan: Plan) {
        context.delete(plan)
        savePlan()
    }
    
    static func retrievePlans() -> [Plan] {
        do {
            let fetchRequest = NSFetchRequest<Plan>(entityName: "Plan")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
