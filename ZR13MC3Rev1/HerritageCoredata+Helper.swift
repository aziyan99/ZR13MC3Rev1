//
//  HerritageCoredata+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import Foundation
import UIKit
import CoreData

struct HerritageCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newHerritage() -> Herritage {
            let herritage = NSEntityDescription.insertNewObject(forEntityName: "Herritage", into: context) as! Herritage
            return herritage
    }
    
    static func saveHerritage() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deleteHerritage(herritage: Herritage) {
        context.delete(herritage)
        saveHerritage()
    }
    
    static func retrieveHerritages() -> [Herritage] {
        do {
            let fetchRequest = NSFetchRequest<Herritage>(entityName: "Herritage")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func filterByPlanId(id: UUID) -> [Herritage] {
        do {
            let predicate = NSPredicate(format: "plan_id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Herritage>(entityName: "Herritage")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func getById(id: UUID) -> [Herritage] {
        do {
            let predicate = NSPredicate(format: "id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Herritage>(entityName: "Herritage")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
