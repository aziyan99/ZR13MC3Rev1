//
//  AddNatureFormViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import UIKit

class AddNatureFormViewController: UIViewController {

    var form: [CreateFormStruct] = [
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .title, form: [
            FormStruct(name: "", handler: {
                print("title")
            })
        ]),
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .datePicker, form: [
            FormStruct(name: "Starts", handler: {
                print("Starts")
            }),
            FormStruct(name: "Ends", handler: {
                print("Ends")
            })
        ]),
        CreateFormStruct(headerTitle: "", footerTitle: "", formType: .contact, form: [
            FormStruct(name: "", handler: {
                print("contact")
            })
        ])
    ]
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    
    var type: String = "nature"
    var startDate: Date!
    var endDate: Date!
    var natureDetail: AddNatureStruct!
    var herritageDetail: AddHerritageStruct!
    var eventDetail: AddEventStruct!
    var planId: UUID!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CreatePlanWithDatePickerTableViewCell.nib(), forCellReuseIdentifier: CreatePlanWithDatePickerTableViewCell.identifier)
        tableView.register(CellWithWeatherButtonTableViewCell.nib(), forCellReuseIdentifier: CellWithWeatherButtonTableViewCell.identifier)
        tableView.register(CellWithContactTableViewCell.nib(), forCellReuseIdentifier: CellWithContactTableViewCell.identifier)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        if type == "nature" {
            let save = NatureCoreDataHelper.newNature()
            save.id = UUID()
            save.icon = "leaf.fill"
            save.name = natureDetail.title!
            save.start = startDate
            save.end = endDate
            save.nature_id = natureDetail._id!
            save.plan_id = planId
            save.lat = natureDetail.latLong[0].lat!
            save.long = natureDetail.latLong[0].long!
            NatureCoreDataHelper.saveNature()
        } else if type == "herritage"{
            let save = HerritageCoreDataHelper.newHerritage()
            save.id = UUID()
            save.icon = "pyramid.fill"
            save.name = herritageDetail.title!
            save.start = startDate
            save.end = endDate
            save.herritage_id = herritageDetail._id!
            save.plan_id = planId
            save.lat = herritageDetail.latLong[0].lat!
            save.long = herritageDetail.latLong[0].long!
            HerritageCoreDataHelper.saveHerritage()
        } else {
            let save = EventCoreDataHelper.newEvent()
            save.id = UUID()
            save.icon = "ticket.fill"
            save.name = eventDetail.title!
            save.start = startDate
            save.end = endDate
            save.event_id = eventDetail._id!
            save.plan_id = planId
            save.lat = 0.918550
            save.long = 104.466507
            EventCoreDataHelper.saveEvent()
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didTapStartsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        startDate = senderDate!.date
    }
    
    @objc func didTapEndsDatePicker(_ sender: Any) {
        let senderDate = sender as? UIDatePicker
        endDate = senderDate!.date
    }
}

extension AddNatureFormViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return form.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return form[section].form.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == "nature" {
            switch form[indexPath.section].formType {
            case .title:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithWeatherButtonTableViewCell.identifier, for: indexPath) as! CellWithWeatherButtonTableViewCell
                cell.configure(title: natureDetail.title!, subtitle: natureDetail.subTitle!, icon: "leaf.fill")
                return cell
            case .datePicker:
                let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithDatePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithDatePickerTableViewCell
                cell.configure(title: form[indexPath.section].form[indexPath.row].name)
                
                if form[indexPath.section].form[indexPath.row].name == "Ends" {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapStartsDatePicker(_ :)), for: .valueChanged)
                } else {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapEndsDatePicker(_ :)), for: .valueChanged)
                }
                
                return cell
            case .contact:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            }
        } else if type == "herritage"{
            switch form[indexPath.section].formType {
            case .title:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithWeatherButtonTableViewCell.identifier, for: indexPath) as! CellWithWeatherButtonTableViewCell
                cell.configure(title: herritageDetail.title!, subtitle: herritageDetail.subTitle!, icon: "leaf.fill")
                return cell
            case .datePicker:
                let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithDatePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithDatePickerTableViewCell
                cell.configure(title: form[indexPath.section].form[indexPath.row].name)
                
                if form[indexPath.section].form[indexPath.row].name == "Ends" {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapStartsDatePicker(_ :)), for: .valueChanged)
                } else {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapEndsDatePicker(_ :)), for: .valueChanged)
                }
                
                return cell
            case .contact:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            }
        } else {
            //event
            switch form[indexPath.section].formType {
            case .title:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithWeatherButtonTableViewCell.identifier, for: indexPath) as! CellWithWeatherButtonTableViewCell
                cell.configure(title: eventDetail.title!, subtitle: eventDetail.subTitle!, icon: "pyramid.fill")
                return cell
            case .datePicker:
                let cell = tableView.dequeueReusableCell(withIdentifier: CreatePlanWithDatePickerTableViewCell.identifier, for: indexPath) as! CreatePlanWithDatePickerTableViewCell
                cell.configure(title: form[indexPath.section].form[indexPath.row].name)
                
                if form[indexPath.section].form[indexPath.row].name == "Ends" {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapStartsDatePicker(_ :)), for: .valueChanged)
                } else {
                    cell.datePickerItem.addTarget(self, action: #selector(didTapEndsDatePicker(_ :)), for: .valueChanged)
                }
                
                return cell
            case .contact:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            }
        }
    }
}

extension AddNatureFormViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch form[indexPath.section].formType {
        case .title: return 108
        case .datePicker: return 90
        case .contact: return 200
        }
    }
}
