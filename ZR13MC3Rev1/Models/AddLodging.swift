//
//  AddLodging.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 06/08/21.
//

import Foundation

struct AddLodgingListResponseStruct: Codable {
    var res: [AddLodgingStruct]
}

struct AddLodgingStruct: Codable {
    var _id: String?
    var title: String?
    var subTitle: String?
    var description: String?
    var image: String?
    var contact: String?
    var website: String?
    var recommendActivities: String?
    var bestView: String?
    var latLong: [LatLong]
    var bestActivities: [bestActivities]
    var nearbyAccomodation: [NearbyAccomodation]
    var nearbyAttraction: [NearbyAttraction]
}

struct LatLong: Codable {
    var lat: Double?
    var long: Double?
}

struct bestActivities: Codable {
    var label: String?
    var kindOf: String?
    var time: String?
}

struct NearbyAccomodation: Codable {
    var label: String?
    var place: String?
    var image: String?
    var duration: String?
}

struct NearbyAttraction: Codable {
    var label: String?
    var availability: String?
    var image: String?
    var description: String?
}
