//
//  AddNature.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import Foundation

struct AddNatureListResponseStruct: Codable {
    var res: [AddLodgingStruct]
}

struct AddNatureStruct: Codable {
    var _id: String?
    var title: String?
    var subTitle: String?
    var description: String?
    var image: String?
    var availableHour: String?
    var latLong: [LatLong]
    var bestActivities: [bestActivities]
    var nearbyAccomodation: [NearbyAccomodation]
    var nearbyAttraction: [NearbyAttraction]
    var nearbyLodging: [NearbyLodging]
}

struct NearbyLodging: Codable {
    var label: String?
    var star: String?
    var location: String?
    var image: String?
}
