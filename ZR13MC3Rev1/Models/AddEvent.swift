//
//  AddEvent.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import Foundation

struct AddEventStruct: Codable {
    var _id: String?
    var title: String?
    var subTitle: String?
    var description: String?
    var image: String?
    var postponed: String?
    var website: String?
    var eventHistory: [EventHistory]
    var recommendedPlan: [RecommendedPlan]
}

struct EventHistory: Codable {
    var firstHeld: String?
    var lastHeld: String?
}

struct RecommendedPlan: Codable {
    var start: String?
    var end: String?
}
