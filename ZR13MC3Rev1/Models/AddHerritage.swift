//
//  AddHerritage.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import Foundation

struct AddHerritageStruct: Codable {
    var _id: String?
    var title: String?
    var subTitle: String?
    var description: String?
    var image: String?
    var availableHour: String?
    var latLong: [LatLong]
    var bestActivities: [bestActivities]
    var nearbyAccomodation: [NearbyAccomodation]
    var nearbyAttraction: [NearbyAttraction]
    var nearbyLodging: [NearbyLodging]
}
