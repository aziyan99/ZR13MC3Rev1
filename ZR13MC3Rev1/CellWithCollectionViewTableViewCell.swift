//
//  CellWithCollectionViewTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithCollectionViewTableViewCell: UITableViewCell {
    static let identifier = "CellWithCollectionViewTableViewCell"
    var nearbyAttraction = [NearbyAttraction]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(CellWithImageCollectionViewCell.nib(), forCellWithReuseIdentifier: CellWithImageCollectionViewCell.identifier)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithCollectionViewTableViewCell", bundle: nil)
    }
    
    public func configure(model: [NearbyAttraction]) {
        nearbyAttraction = model
    }
}

extension CellWithCollectionViewTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nearbyAttraction.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellWithImageCollectionViewCell.identifier, for: indexPath) as! CellWithImageCollectionViewCell
        cell.configure(imageUrl: nearbyAttraction[indexPath.row].image!, title: nearbyAttraction[indexPath.row].label!, subtitle: nearbyAttraction[indexPath.row].description!)
        return cell
    }
}
