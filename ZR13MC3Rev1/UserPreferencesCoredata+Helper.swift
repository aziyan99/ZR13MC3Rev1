//
//  UserPreferencesCoredata+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import Foundation
import UIKit
import CoreData

struct UserPreferencesCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newUserPreferences() -> UserPreferences {
            let data = NSEntityDescription.insertNewObject(forEntityName: "UserPreferences", into: context) as! UserPreferences
            return data
    }
    
    static func saveUserPreferences() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deleteUserPreferences(userPreferences: UserPreferences) {
        context.delete(userPreferences)
        saveUserPreferences()
    }
    
    static func retrieveUserPreferences() -> [UserPreferences] {
        do {
            let fetchRequest = NSFetchRequest<UserPreferences>(entityName: "UserPreferences")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func filterByName(name: String) -> [UserPreferences] {
        do {
            let predicate = NSPredicate(format: "name == %@", name)
            let fetchRequest = NSFetchRequest<UserPreferences>(entityName: "UserPreferences")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
