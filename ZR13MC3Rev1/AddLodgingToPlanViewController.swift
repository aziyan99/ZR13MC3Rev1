//
//  AddLodgingToPlanViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import UIKit
import MapKit

class AddLodgingToPlanViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var filterContainerView: UIView!
    @IBOutlet weak var filterNameLabel: UILabel!
    
    var planId: UUID!
    var filterId: String = ""
    var filterType: String = ""
    
    var lodgingCoordinate = [MapViewStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        
        filterContainerView.dropShadow()
        filterContainerView.layer.cornerRadius = 5
        
        DispatchQueue.main.async { [self] in
            let lodgingListViewController = LodgingListInAddLodgingViewController()
            lodgingListViewController.modalPresentationStyle = .custom
            lodgingListViewController.transitioningDelegate = self
            lodgingListViewController.planId = planId
            self.present(lodgingListViewController, animated: true, completion: nil)
            
            self.fetchLodgingList {
                // mapping lodging pin to the map
                var annotation = [MKAnnotation]()
                for item in lodgingCoordinate {
                    let artwork = Artwork(
                        title: item.title,
                        locationName: item.locationName,
                      discipline: "Sculpture",
                      coordinate: CLLocationCoordinate2D(latitude: item.lat!, longitude: item.long!))
                    annotation.append(artwork)
                }
                mapView.showAnnotations(annotation, animated: true)
            }
            
            mapView.mapType = MKMapType.standard
            mapView.isZoomEnabled = true
            mapView.isScrollEnabled = true
            mapView.delegate = self
        }
    }
    
    private func fetchLodgingList(completionHandler: @escaping () -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/lodgings")!, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print("error")
                return
            }
            
            //have data
            var response: [AddLodgingStruct]!
            do {
                response = try JSONDecoder().decode([AddLodgingStruct].self, from: data)
                for lodging in response {
                    self.lodgingCoordinate.append(MapViewStruct(lat: lodging.latLong[0].lat, long: lodging.latLong[0].long, title: lodging.title, locationName: lodging.subTitle))
                }
                DispatchQueue.main.async {
                    completionHandler()
                }
            } catch {
                print("Failed to convert \(error.localizedDescription)")
            }
        })
        task.resume()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        navigationController?.navigationBar.sizeToFit()
        navigationController?.navigationBar.tintColor = .systemBlue
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapFilterButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LodgingFilter", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "LodgingFilterViewController") as! LodgingFilterViewController
        viewController.filterDelegate = self
        present(viewController, animated: true, completion: nil)
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddLodgingToPlanViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        PresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension AddLodgingToPlanViewController: MKMapViewDelegate {
    // 1
    func mapView(
        _ mapView: MKMapView,
        viewFor annotation: MKAnnotation
    ) -> MKAnnotationView? {
        // 2
        guard let annotation = annotation as? Artwork else {
            return nil
        }
        // 3
        let identifier = "artwork"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView (
            withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(
                annotation: annotation,
                reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
}

extension AddLodgingToPlanViewController: FilterChooseDelegate {
    func didTapChoice(itemName: String, itemId: String, itemType: String) {
        filterNameLabel.text = itemName
        viewDidLoad()
    }
}

