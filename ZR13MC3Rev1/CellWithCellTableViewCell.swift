//
//  CellWithCellTableViewCell.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 10/08/21.
//

import UIKit

class CellWithCellTableViewCell: UITableViewCell {

    static let identifier = "CellWithCellTableViewCell"
    
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageContainerView.layer.cornerRadius = 5
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CellWithCellTableViewCell", bundle: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
    }
    
    public func configure(icon: String, title: String, subtitle: String) {
        iconImageView.image = UIImage(systemName: icon)
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
}
