//
//  NatureDetailsInAddNatureViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 11/08/21.
//

import UIKit

struct AddNatureStructHelpers {
    var header: String
    var type: AddNatureStructEnumHelpers
}

enum AddNatureStructEnumHelpers {
    case image
    case desc
    case activities
    case contact
    case map
    case accomodation
    case attraction
}

struct AddEventStructHelpers {
    var header: String
    var type: AddEventStructEnumHelpers
}

enum AddEventStructEnumHelpers {
    case image
    case desc
    case history
    case website
    case recommendedPlan
}

class NatureDetailsInAddNatureViewController: UIViewController {
    
    var items: [AddNatureStructHelpers] = [
        AddNatureStructHelpers(header: "", type: .image),
        AddNatureStructHelpers(header: "", type: .desc),
        AddNatureStructHelpers(header: "", type: .activities),
        AddNatureStructHelpers(header: "", type: .contact),
        AddNatureStructHelpers(header: "", type: .map),
        AddNatureStructHelpers(header: "Nearby Accomodations", type: .accomodation),
        AddNatureStructHelpers(header: "Nearby Attractions", type: .attraction)
    ]
    
    var eventItems: [AddEventStructHelpers] = [
        AddEventStructHelpers(header: "", type: .image),
        AddEventStructHelpers(header: "", type: .desc),
        AddEventStructHelpers(header: "", type: .history),
        AddEventStructHelpers(header: "", type: .website),
        AddEventStructHelpers(header: "", type: .recommendedPlan)
    ]
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    
    var natureDetail: AddNatureStruct!
    var herritageDetail: AddHerritageStruct!
    var eventDetail: AddEventStruct!
    var type: String = "nature"
    var planId: UUID!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newActivityAdded"), object: nil)

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CellWithImageTableViewCell.self, forCellReuseIdentifier: CellWithImageTableViewCell.identifier)
        tableView.register(CellWithDescTableViewCell.self, forCellReuseIdentifier: CellWithDescTableViewCell.identifier)
        tableView.register(CellWithBestActivitiesTodoTableViewCell.nib(), forCellReuseIdentifier: CellWithBestActivitiesTodoTableViewCell.identifier)
        tableView.register(CellWithContactTableViewCell.nib(), forCellReuseIdentifier: CellWithContactTableViewCell.identifier)
        tableView.register(CellWithMapTableViewCell.nib(), forCellReuseIdentifier: CellWithMapTableViewCell.identifier)
        tableView.register(CellWithCellTableViewCell.nib(), forCellReuseIdentifier: CellWithCellTableViewCell.identifier)
        tableView.register(CellWithCollectionViewTableViewCell.nib(), forCellReuseIdentifier: CellWithCollectionViewTableViewCell.identifier)
        tableView.register(CellWithImageAndSubtitleTableViewCell.nib(), forCellReuseIdentifier: CellWithImageAndSubtitleTableViewCell.identifier)
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapAddActivityButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AddNatureForm", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "AddNatureFormViewController") as! AddNatureFormViewController
        viewController.planId = planId
        if type == "nature" {
            viewController.type = "nature"
            viewController.natureDetail = natureDetail
        } else if type == "herritage" {
            viewController.type = "herritage"
            viewController.herritageDetail = herritageDetail
        } else {
            //event
            viewController.type = "event"
            viewController.eventDetail = eventDetail
        }
        self.present(viewController, animated: true, completion: nil)
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NatureDetailsInAddNatureViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if type == "nature" || type == "herritage" {
            return 7
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == "herritage" || type == "nature" {
            switch items[section].type {
            case .image:
                return 1
            case .desc:
                return 1
            case .activities:
                return 1
            case .contact:
                return 1
            case .map:
                return 1
            case .accomodation:
                if type == "nature" {
                    return natureDetail.nearbyAccomodation.count
                } else {
                    return herritageDetail.nearbyAccomodation.count
                }
            case .attraction:
                if type == "nature" {
                    return natureDetail.nearbyAttraction.count
                } else {
                    return herritageDetail.nearbyAttraction.count
                }
            }
        } else {
            //event
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == "nature" {
            switch items[indexPath.section].type {
            case .image:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithImageAndSubtitleTableViewCell.identifier, for: indexPath) as! CellWithImageAndSubtitleTableViewCell
                cell.configure(imageUrl: natureDetail.image!, title: natureDetail.title!, subtitle: natureDetail.availableHour!)
                return cell
            case .desc:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithDescTableViewCell.identifier, for: indexPath) as! CellWithDescTableViewCell
                cell.configure(title: natureDetail.title!, subtitle: natureDetail.description!)
                return cell
            case .activities:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithBestActivitiesTodoTableViewCell.identifier, for: indexPath) as! CellWithBestActivitiesTodoTableViewCell
                
                return cell
            case .contact:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            case .map:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithMapTableViewCell.identifier, for: indexPath) as! CellWithMapTableViewCell
                cell.configure(name: natureDetail.title!, lat: natureDetail.latLong[0].lat!, long: natureDetail.latLong[0].long!)
                return cell
            case .accomodation:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCellTableViewCell.identifier, for: indexPath) as! CellWithCellTableViewCell
                cell.configure(icon: natureDetail.nearbyAccomodation[indexPath.row].image!, title: natureDetail.nearbyAccomodation[indexPath.row].label!, subtitle: natureDetail.nearbyAccomodation[indexPath.row].place!)
                return cell
            case .attraction:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCollectionViewTableViewCell.identifier, for: indexPath) as! CellWithCollectionViewTableViewCell
                cell.configure(model: natureDetail.nearbyAttraction)
                return cell
            }
        } else if type == "herritage" {
            switch items[indexPath.section].type {
            case .image:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithImageAndSubtitleTableViewCell.identifier, for: indexPath) as! CellWithImageAndSubtitleTableViewCell
                cell.configure(imageUrl: herritageDetail.image!, title: herritageDetail.title!, subtitle: herritageDetail.availableHour!)
                return cell
            case .desc:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithDescTableViewCell.identifier, for: indexPath) as! CellWithDescTableViewCell
                cell.configure(title: herritageDetail.title!, subtitle: herritageDetail.description!)
                return cell
            case .activities:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithBestActivitiesTodoTableViewCell.identifier, for: indexPath) as! CellWithBestActivitiesTodoTableViewCell
                
                return cell
            case .contact:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            case .map:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithMapTableViewCell.identifier, for: indexPath) as! CellWithMapTableViewCell
                cell.configure(name: herritageDetail.title!, lat: herritageDetail.latLong[0].lat!, long: herritageDetail.latLong[0].long!)
                return cell
            case .accomodation:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCellTableViewCell.identifier, for: indexPath) as! CellWithCellTableViewCell
                cell.configure(icon: herritageDetail.nearbyAccomodation[indexPath.row].image!, title: herritageDetail.nearbyAccomodation[indexPath.row].label!, subtitle: herritageDetail.nearbyAccomodation[indexPath.row].place!)
                return cell
            case .attraction:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithCollectionViewTableViewCell.identifier, for: indexPath) as! CellWithCollectionViewTableViewCell
                cell.configure(model: herritageDetail.nearbyAttraction)
                return cell
            }
        } else {
            //event
            switch eventItems[indexPath.section].type {
            case .image:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithImageAndSubtitleTableViewCell.identifier, for: indexPath) as! CellWithImageAndSubtitleTableViewCell
                cell.configure(imageUrl: eventDetail.image!, title: eventDetail.title!, subtitle: eventDetail.postponed!)
                return cell
            case .desc:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithDescTableViewCell.identifier, for: indexPath) as! CellWithDescTableViewCell
                cell.configure(title: eventDetail.subTitle!, subtitle: eventDetail.description!)
                return cell
            case .history:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            case .website:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            case .recommendedPlan:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellWithContactTableViewCell.identifier, for: indexPath) as! CellWithContactTableViewCell
                cell.configure(phoneNumber: "+62 2254 3322", website: "nature.com")
                return cell
            }
        }
    }
}

extension NatureDetailsInAddNatureViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].header
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if type == "nature" || type == "herritage" {
            switch items[indexPath.section].type {
            case .image:
                return 613
            case .desc:
                return 284
            case .activities:
                return 280
            case .contact:
                return 200
            case .map:
                return 200
            case .accomodation:
                return 98
            case .attraction:
                return 226
            }
        }else{
            //event
            switch eventItems[indexPath.section].type {
            case .image:
                return 613
            case .desc:
                return 284
            case .history:
                return 284
            case .website:
                return 284
            case .recommendedPlan:
                return 284
            }
        }
    }
}
