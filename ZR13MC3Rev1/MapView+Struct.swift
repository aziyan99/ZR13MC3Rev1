//
//  MapView+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 08/08/21.
//

import Foundation


struct MapViewStruct {
    var lat: Double?
    var long: Double?
    var title: String?
    var locationName: String?
}
