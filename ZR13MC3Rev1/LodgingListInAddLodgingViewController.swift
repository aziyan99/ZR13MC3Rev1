//
//  LodgingListInAddLodgingViewController.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 04/08/21.
//

import UIKit

class LodgingListInAddLodgingViewController: UIViewController {
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    var lodgingLists = [AddLodgingStruct]()
    var planId: UUID!
    
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(closePage), name: NSNotification.Name(rawValue: "newLodgingAdded"), object: nil)
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addGestureRecognizer(panGesture)
        
        headerContainer.roundCorners([.topLeft, .topRight], radius: 10)
        
        fetchLodgingList {
            self.tableView.reloadData()
        }
        
        
        tableView.register(LodgingListInAddLodgingTableViewCellTableViewCell.nib(), forCellReuseIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    private func fetchLodgingList(completionHandler: @escaping () -> ()) {
        let task = URLSession.shared.dataTask(with: URL(string: "http://bintan.iniserver.xyz/lodgings")!, completionHandler: { (data, response, error) in
            guard let data = data, error == nil else {
                print("error")
                return
            }
            
            //have data
            do {
                self.lodgingLists = try JSONDecoder().decode([AddLodgingStruct].self, from: data)
                DispatchQueue.main.async {
                    completionHandler()
                }
            } catch {
                print("Failed to convert \(error.localizedDescription)")
            }
        })
        task.resume()
    }
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)

        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }

        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)

        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
        
        // let translation = sender.translation(in: self.view)
        // let y = self.view.frame.minY
        // self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
        // sender.setTranslation(.zero, in: self.view)
    }
    
    @objc func closePage () {
        self.dismiss(animated: true, completion: nil)
    }
}

extension LodgingListInAddLodgingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lodgingLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LodgingListInAddLodgingTableViewCellTableViewCell.identifier, for: indexPath) as! LodgingListInAddLodgingTableViewCellTableViewCell
        cell.configure(title: lodgingLists[indexPath.row].title!, subtitle: lodgingLists[indexPath.row].title!)
        return cell
    }
}

extension LodgingListInAddLodgingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("select")
        let detailLodgingStoryboard = UIStoryboard(name: "LodgingDetailsInAddLodging", bundle: nil)
        let detailLodgingViewController = detailLodgingStoryboard.instantiateViewController(identifier: "LodgingDetailsInAddLodgingViewController") as! LodgingDetailsInAddLodgingViewController
        detailLodgingViewController.lodgingDetail = lodgingLists[indexPath.row]
        detailLodgingViewController.planId = planId
        present(detailLodgingViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
}
