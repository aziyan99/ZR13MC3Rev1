//
//  RestructurePlan+Struct.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 02/08/21.
//

import Foundation

struct RestructurePlan {
    var year: String?
    var plans: [Plan]?
}
