//
//  LogingCoredata+Helper.swift
//  ZR13MC3Rev1
//
//  Created by Raja Azian on 09/08/21.
//

import Foundation
import UIKit
import CoreData

struct LodgingCoreDataHelper {
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    static func newLodging() -> Lodging {
            let lodging = NSEntityDescription.insertNewObject(forEntityName: "Lodging", into: context) as! Lodging
            return lodging
    }
    
    static func saveLodging() {
        do {
            try context.save()
        } catch let error {
            print("Could not save \(error.localizedDescription)")
        }
    }
    
    static func deleteLodging(lodging: Lodging) {
        context.delete(lodging)
        saveLodging()
    }
    
    static func retrieveLodgings() -> [Lodging] {
        do {
            let fetchRequest = NSFetchRequest<Lodging>(entityName: "Lodging")
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func filterByPlanId(id: UUID) -> [Lodging] {
        do {
            let predicate = NSPredicate(format: "plan_id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Lodging>(entityName: "Lodging")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
    
    static func getById(id: UUID) -> [Lodging] {
        do {
            let predicate = NSPredicate(format: "id == %@", id as CVarArg)
            let fetchRequest = NSFetchRequest<Lodging>(entityName: "Lodging")
            fetchRequest.predicate = predicate
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("Could not fetch \(error.localizedDescription)")
            return []
        }
    }
}
